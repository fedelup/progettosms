package it.uniba.di.sms.gruppo13.gruppo13.session.adapters;

import java.io.Serializable;
import java.util.ArrayList;

public class ParentSessionItem implements Serializable {

    private String parentName;
    private ArrayList<ChildSessionItem> childSessionItems;

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public ArrayList<ChildSessionItem> getChildSessionItems() {
        return childSessionItems;
    }

    public void setChildSessionItems(ArrayList<ChildSessionItem> childSessionItems) {
        this.childSessionItems = childSessionItems;
    }
}
