package it.uniba.di.sms.gruppo13.gruppo13;

import android.content.Context;
import android.support.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import it.uniba.di.sms.gruppo13.gruppo13.data.DataStorage;
import it.uniba.di.sms.gruppo13.gruppo13.data.ManagerData;

public class EstimateDriver {

    private int totInfrazioni;
    private int nRilevazioni;
    private ManagerData managerData;

    public EstimateDriver(Context context) {
        managerData = new ManagerData(context);
        if (managerData.getCurrentUser() != null)
            getDBValutation();
        else {
            DataStorage dataStorage = new DataStorage(context);
            nRilevazioni = dataStorage.loadRilevazioni();
            totInfrazioni = dataStorage.loadTotInfraction();
        }
    }

    public int getTotInfrazioni() {
        return totInfrazioni;
    }

    public int getnRilevazioni() {
        return nRilevazioni;
    }

    private void getDBValutation() {
        DatabaseReference myRef = managerData.getRefValutation();
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    nRilevazioni = Integer.valueOf(dataSnapshot.child("nRilevazioni").getValue().toString());
                    totInfrazioni = Integer.valueOf(dataSnapshot.child("totInfrazioni").getValue().toString());
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {

            }
        });
    }

}

