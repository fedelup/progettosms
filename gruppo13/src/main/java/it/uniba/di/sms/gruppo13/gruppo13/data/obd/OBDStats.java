package it.uniba.di.sms.gruppo13.gruppo13.data.obd;

import java.io.Serializable;
import java.util.Random;

public class OBDStats implements Serializable {
    private double GPSLongitude;
    private double GPSLatitude;
    private double OBDSpeed;
    private int engineRPM;
    private double engineLoad;
    private double fuelFlowRate;
    private double throttlePosition = 0;
    private double relativeThrottlePosition = 0;
    private double acceleratorPedalRelativePosition;
    private int pendenzaStrada;
    private boolean rettilineo;
    private String tipoStrada;


    public OBDStats(String GPSLongitude, String GPSLatitude, String OBDSpeed,
                    String engineRPM, String engineLoad, String fuelFlowRate,
                    String acceleratorPedalRelativePosition, String pendenzaSrada, String rettilineo,
                    String tipoStrada) {
        this.GPSLongitude = Double.valueOf(GPSLongitude);
        this.GPSLatitude = Double.valueOf(GPSLatitude);
        this.OBDSpeed = Double.valueOf(OBDSpeed);
        this.engineRPM = Integer.valueOf(engineRPM);
        this.engineLoad = Double.valueOf(engineLoad);
        this.fuelFlowRate = Double.valueOf(fuelFlowRate);
        this.acceleratorPedalRelativePosition = Double.valueOf(acceleratorPedalRelativePosition);
        this.pendenzaStrada = Integer.valueOf(pendenzaSrada);
        this.rettilineo = rettilineo.toLowerCase().equals("true");
        this.tipoStrada = tipoStrada;
    }


    public void calculateEngineLoad() {
        engineLoad = ((OBDSpeed * 100 / 255) * 2) + ((engineRPM * 100 / 16383) * 5) +
                ((acceleratorPedalRelativePosition) * 3) / 10;
    }

    public void randomSetLongitude(double min, double max) {
        Random random = new Random();
        do {
            GPSLongitude = min;
            GPSLongitude += random.nextGaussian() / 10;
        } while (GPSLongitude < min && GPSLongitude > max);
    }

    public void randomSetLatitude(double min, double max) {
        Random random = new Random();
        do {
            GPSLatitude = min;
            GPSLatitude += random.nextGaussian() / 10;
        } while (GPSLatitude < min && GPSLatitude > max);
    }


    public void randomGPSSpeed(int increment, double prevSpeed) {
        Random random = new Random();
        if (increment == 0) {
            OBDSpeed = prevSpeed;
        } else if (increment < 0) {
            OBDSpeed = prevSpeed + increment;
        } else if (prevSpeed >= 130) {
            OBDSpeed = random.nextInt(5) + prevSpeed;
        } else if (prevSpeed < 130) {
            OBDSpeed = increment + prevSpeed;
        }


        if (OBDSpeed >= 255)
            OBDSpeed = 255;
        if (OBDSpeed <= 0)
            OBDSpeed = 0;
        calculateAcceleratorPedal(prevSpeed);

    }

    public void randomEngineRPM(int increment, int prevRPM) {
        if (increment == 0)
            engineRPM = prevRPM;
        else
            engineRPM = prevRPM + (increment * 10);
        if (engineRPM >= 16383)
            engineRPM = 16383;
        if (engineRPM < 0)
            engineRPM = 0;
    }


    public void calculateFuelRate() {
        fuelFlowRate = (this.getOBDSpeed() * 30 * 4 / 255) + ((this.getEngineRPM() * 20 * 6) / 16383) / 10;
    }

    public void randomFuelRate(int increment, double prevFuelRate) {
        if (increment == 0)
            fuelFlowRate = prevFuelRate - (prevFuelRate * 0.08);
        else
            fuelFlowRate = prevFuelRate + (increment * 10);


        if (fuelFlowRate > 3276)
            fuelFlowRate = 3276;
        if (fuelFlowRate < 0)
            fuelFlowRate = 0;
    }


    private void calculateAcceleratorPedal(double prevSpeed) {
        if (getOBDSpeed() == 0)
            acceleratorPedalRelativePosition = 0;
        else if (prevSpeed <= 40)
            acceleratorPedalRelativePosition = ((Math.abs(prevSpeed - getOBDSpeed())) * 100 / (prevSpeed + 28));
        else if (prevSpeed > 41)
            acceleratorPedalRelativePosition = 32 + ((Math.abs(prevSpeed - getOBDSpeed())) * 100 / prevSpeed);
    }

    public double getGPSLongitude() {
        return GPSLongitude;
    }

    public String toStringGPSLongitude() {
        return String.valueOf(GPSLongitude);
    }

    public void setGPSLongitude(double gPSLongitude) {
        GPSLongitude = gPSLongitude;
    }

    public double getGPSLatitude() {
        return GPSLatitude;
    }

    public String toStringGPSLatitude() {
        return String.valueOf(GPSLatitude);
    }

    public void setGPSLatitude(double gPSLatitude) {
        GPSLatitude = gPSLatitude;
    }

    public double getOBDSpeed() {
        return OBDSpeed;
    }

    public String toStringOBDSpeed() {
        return String.valueOf(OBDSpeed);
    }

    public void setOBDSpeed(double obdSpeed) {
        OBDSpeed = obdSpeed;
    }

    public double getEngineLoad() {
        return engineLoad;
    }

    public String toStringEngineLoad() {
        return String.valueOf(engineLoad);
    }

    public void setEngineLoad(double engineLoad) {
        this.engineLoad = engineLoad;
    }

    public double getFuelFlowRate() {
        return fuelFlowRate;
    }

    public String toStringFuelFlowRate() {
        return String.valueOf(fuelFlowRate);
    }

    public void setFuelFlowRate(double fuelFlowRate) {
        this.fuelFlowRate = fuelFlowRate;
    }

    public double getThrottlePosition() {
        return throttlePosition;
    }

    public String toStringThrottlePosition() {
        return String.valueOf(throttlePosition);
    }

    public void setThrottlePosition(double throttlePosition) {
        this.throttlePosition = throttlePosition;
    }

    public double getRelativeThrottlePosition() {
        return relativeThrottlePosition;
    }

    public String toStringRelativeThrottlePosition() {
        return String.valueOf(relativeThrottlePosition);
    }

    public void setRelativeThrottlePosition(double relativeThrottlePosition) {
        this.relativeThrottlePosition = relativeThrottlePosition;
    }

    public double getAcceleratorPedalRelativePosition() {
        return acceleratorPedalRelativePosition;
    }

    public String toStringAcceleratorPedalRelativePosition() {
        return String.valueOf(acceleratorPedalRelativePosition);
    }

    public void setAcceleratorPedalRelativePosition(double acceleratorPedalRelativePosition) {
        this.acceleratorPedalRelativePosition = acceleratorPedalRelativePosition;
    }

    public int getEngineRPM() {
        return engineRPM;
    }

    public String toStringEngineRPM() {
        return String.valueOf(engineRPM);
    }

    public void setEngineRPM(int engineRPM) {
        this.engineRPM = engineRPM;
    }

    public int getPendenzaStrada() {
        return pendenzaStrada;
    }

    public String toStringPendenzaStrada() {
        return String.valueOf(pendenzaStrada);
    }

    public void setPendenzaStrada(int pendenzaStrada) {
        this.pendenzaStrada = pendenzaStrada;
    }

    public boolean isRettilineo() {
        return rettilineo;
    }

    public void setRettilineo(boolean rettilineo) {
        this.rettilineo = rettilineo;
    }

    public String toStringRettilineo() {
        if (isRettilineo()) {
            return "true";
        } else {
            return "false";
        }
    }

    public String getTipoStrada() {
        return tipoStrada;
    }

    public void setTipoStrada(String tipoStrada) {
        this.tipoStrada = tipoStrada;
    }

    @Override
    public String toString() {
        return "OBDStats{" +
                "GPSLongitude=" + GPSLongitude +
                ", GPSLatitude=" + GPSLatitude +
                ", OBDSpeed=" + OBDSpeed +
                ", engineRPM=" + engineRPM +
                ", engineLoad=" + engineLoad +
                ", fuelFlowRate=" + fuelFlowRate +
                ", throttlePosition=" + throttlePosition +
                ", relativeThrottlePosition=" + relativeThrottlePosition +
                ", acceleratorPedalRelativePosition=" + acceleratorPedalRelativePosition +
                ", pendenzaStrada=" + pendenzaStrada +
                ", rettilineo=" + rettilineo +
                ", tipoStrada='" + tipoStrada + '\'' +
                '}';
    }
}
