package it.uniba.di.sms.gruppo13.gruppo13.car;


import android.app.ProgressDialog;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;

import android.view.MenuItem;
import android.view.View;
import android.view.animation.OvershootInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import it.uniba.di.sms.gruppo13.gruppo13.car.adapter.CarInfoAdapter;
import it.uniba.di.sms.gruppo13.gruppo13.R;
import it.uniba.di.sms.gruppo13.gruppo13.data.DataStorage;
import it.uniba.di.sms.gruppo13.gruppo13.data.ManagerData;
import it.uniba.di.sms.gruppo13.gruppo13.user.MainActivity;
import jp.wasabeef.recyclerview.animators.FlipInRightYAnimator;

import static android.content.ContentValues.TAG;

public class GarageActivity extends AppCompatActivity implements View.OnClickListener {

    private CarInfoAdapter adapter;
    private FloatingActionButton fabBottom;
    private ProgressDialog progressDialog;
    private CoordinatorLayout coordinatorLayout;
    private ManagerData managerData;
    private List<Car> carList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.exit, R.anim.entry2);
        setContentView(R.layout.activity_garage);

        managerData = new ManagerData(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_activity);
        setSupportActionBar(toolbar);
        initCollapsingToolbar();
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        adapter = new CarInfoAdapter(this, carList, getSupportFragmentManager());

        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.main_content);
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 1);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(1, dpToPx(0), true));
        FlipInRightYAnimator animator = new FlipInRightYAnimator(new OvershootInterpolator(1f));
        recyclerView.setItemAnimator(animator);
        recyclerView.setAdapter(adapter);

        if (managerData.getCurrentUser() != null) {
            loadEntries();
        } else {
            DataStorage dataStorage = new DataStorage(this);
            Car car = dataStorage.loadCar();
            if (car != null)
                carList.add(dataStorage.loadCar());
        }


        try {
            Glide.with(this).load(R.drawable.carpark).into((ImageView) findViewById(R.id.imageView));
        } catch (Exception e) {
            e.printStackTrace();
        }

        FloatingActionButton fabTop = (FloatingActionButton) findViewById(R.id.add_car);
        fabTop.setOnClickListener(this);

        fabBottom = (FloatingActionButton) findViewById(R.id.bottom_add_car);
        fabBottom.setOnClickListener(this);

    }

    /**
     * to create icon go back
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return false;
    }

    /**
     * Initializing collapsing toolbar
     * Will show_session_dialog and hide the toolbar title on scroll
     */
    private void initCollapsingToolbar() {
        final CollapsingToolbarLayout collapsingToolbar =
                (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbar.setTitle(" ");
        AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.appbar);
        appBarLayout.setExpanded(true);


        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = false;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }

                if (scrollRange + verticalOffset == 0) {
                    if (managerData.getCurrentUser() != null) {
                        collapsingToolbar.setTitle(getString(R.string.app_name));
                    } else
                        collapsingToolbar.setTitle(getString(R.string.app_name) + " (Demo)");
                    fabBottom.show();
                    isShow = true;
                } else if (isShow) {
                    collapsingToolbar.setTitle("");
                    fabBottom.hide();
                    isShow = false;
                }
            }
        });
    }

    /**
     * RecyclerView item decoration - give equal margin around grid item
     */
    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

    private void loadEntries() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle(getString(R.string.progress_dialog_title));
        progressDialog.setMessage(getString(R.string.progress_dialog_message));
        progressDialog.setMax(50);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();
        // Read from the database
        DatabaseReference myRef = managerData.getRefDbCar();
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    carList.clear();
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        Car car = new Car(
                                snapshot.getKey().trim(),
                                Objects.requireNonNull(snapshot.child("brand").getValue()).toString(),
                                Objects.requireNonNull(snapshot.child("model").getValue()).toString().trim(),
                                Objects.requireNonNull(snapshot.child("nickname").getValue()).toString().trim());
                        carList.add(car);
                    }
                    adapter.notifyItemRangeChanged(0, carList.size());
                } else {
                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, R.string.snack_bar_text, Snackbar.LENGTH_LONG);
                    View snackbarView = snackbar.getView();
                    TextView textView = snackbarView.findViewById(android.support.design.R.id.snackbar_text);
                    textView.setTextColor(Color.WHITE);
                    snackbar.show();
                }
                progressDialog.dismiss();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                // Failed to read value
                Log.w(TAG, getString(R.string.errore_lettura_valori), error.toException());
                progressDialog.dismiss();
            }
        });
    }

    @Override
    public void onClick(View view) {

        FragmentManager fragmentManager = getSupportFragmentManager();
        DialogNewCar newDialogNewCar = new DialogNewCar();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        //transaction.setTransition(FragmentTransaction.TRANSIT_ENTER_MASK);
        transaction.setCustomAnimations(R.anim.activity_open_enter, R.anim.activity_open_exit);
        transaction.add(R.id.main_content, newDialogNewCar).addToBackStack(null).commit();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(GarageActivity.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }


}




