package it.uniba.di.sms.gruppo13.gruppo13.data;

import android.content.Context;
import android.util.Log;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

import it.uniba.di.sms.gruppo13.gruppo13.car.Car;
import it.uniba.di.sms.gruppo13.gruppo13.session.Session;

public class DataStorage {

    private String fileNameCar = "car_file.txt";
    private String fileNameSession = "session_file.txt";
    private String fileTotInfraction = "infraction_file.txt";
    private String fileTotDetection = "rilevazioini_file.txt";
    private Context context;

    public DataStorage(Context context) {
        this.context = context;
    }

    public void saveCar(Car car) {
        try {
            FileOutputStream fos = context.openFileOutput(fileNameCar, Context.MODE_PRIVATE);
            ObjectOutputStream os = new ObjectOutputStream(fos);
            os.writeObject(car);
            os.close();
            fos.close();
        } catch (Exception e) {
            Log.d("exception save", e.getMessage());
        }

    }

    public Car loadCar() {
        Car car = null;
        try {
            FileInputStream fis = context.openFileInput(fileNameCar);
            ObjectInputStream is = new ObjectInputStream(fis);
            car = (Car) is.readObject();
            is.close();
            fis.close();
        } catch (Exception e) {
            Log.d("exception load", e.getMessage());
        }
        return car;
    }

    public void saveTotInfraction(int totInfractions) {
        try {
            FileOutputStream fOut = context.openFileOutput(fileTotInfraction, Context.MODE_PRIVATE);
            OutputStreamWriter osw = new OutputStreamWriter(fOut);
            osw.write(totInfractions);
            osw.flush();
            osw.close();
            fOut.close();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

    }

    public int loadTotInfraction() {
        int value = 0;
        try {
            FileInputStream fIn = context.openFileInput(fileTotInfraction);
            InputStreamReader isr = new InputStreamReader(fIn);
            value = isr.read();
            fIn.close();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return value;
    }

    public void saveRilevazioni(int rilevazioni) {
        try {
            FileOutputStream fOut = context.openFileOutput(fileTotDetection, Context.MODE_PRIVATE);
            OutputStreamWriter osw = new OutputStreamWriter(fOut);
            osw.write(rilevazioni);
            osw.flush();
            osw.close();
            fOut.close();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

    }

    public int loadRilevazioni() {
        int value = 0;
        try {
            FileInputStream fIn = context.openFileInput(fileTotDetection);
            InputStreamReader isr = new InputStreamReader(fIn);
            value = isr.read();
            fIn.close();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

        return value;
    }

    /**
     * salvataggio sessione in un file
     *
     * @param s
     * @return true - sessione salvata
     * false - sessione non salvata
     */
    public boolean saveSession(Session s) {
        ArrayList<Session> sessions = loadSession();
        Log.d("sessions", sessions.toString());
        if (sessions.size() < 5) {
            sessions.add(s);
            try {
                FileOutputStream fos = context.openFileOutput(fileNameSession, Context.MODE_PRIVATE);
                ObjectOutputStream os = new ObjectOutputStream(fos);
                os.writeObject(sessions);
                os.close();
                fos.close();
            } catch (Exception e) {
                Log.d("exception", e.getMessage());
            }
            return true;
        } else
            return false;

    }

    public ArrayList<Session> loadSession() {
        ArrayList<Session> sessions = new ArrayList<>();
        try {
            FileInputStream fis = context.openFileInput(fileNameSession);
            ObjectInputStream is = new ObjectInputStream(fis);
            sessions = (ArrayList<Session>) is.readObject();
            is.close();
            fis.close();
        } catch (Exception e) {
            Log.d("exception", e.getMessage());
        }
        return sessions;
    }

    public boolean deleteCarFile() {
        context.deleteFile(fileNameCar);
        Log.e("Delete", "File deleted.");
        return true;
    }

    public void resetFile() {
        String[] strings = {fileTotInfraction,fileNameCar,fileNameSession, fileTotDetection};
        for (String nFile : strings) {
            context.deleteFile(nFile);
        }
    }
}
