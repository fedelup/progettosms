package it.uniba.di.sms.gruppo13.gruppo13.user;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LegendEntry;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.utils.MPPointF;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import it.uniba.di.sms.gruppo13.gruppo13.R;
import it.uniba.di.sms.gruppo13.gruppo13.data.DataStorage;
import it.uniba.di.sms.gruppo13.gruppo13.data.ManagerData;
import it.uniba.di.sms.gruppo13.gruppo13.data.obd.OBDStats;
import it.uniba.di.sms.gruppo13.gruppo13.session.Session;

import static android.content.ContentValues.TAG;
import static com.github.mikephil.charting.utils.ColorTemplate.*;


public class DialogShowSession extends DialogFragment {

    private ManagerData managerData;
    private DataStorage dataStorage;
    private View rootView;
    private ProgressDialog progressDialog;

    private ArrayList<Double> gpsLongitude = new ArrayList<>();
    private ArrayList<Double> gpsLatitude = new ArrayList<>();
    private ArrayList<Integer> obdSpeed = new ArrayList<>();
    private ArrayList<Integer> engineRPM = new ArrayList<>();
    private ArrayList<Double> acceleratorPedalRelativePosition = new ArrayList<>();
    private ArrayList<Double> engineLoad = new ArrayList<>();
    private ArrayList<Double> fuelFlowRate = new ArrayList<>();
    private ArrayList<String> tipoStrada = new ArrayList<>();
    private ArrayList<Boolean> rettilineo = new ArrayList<>();
    private ArrayList<Integer> pendenza = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        managerData = new ManagerData(this.getContext());
        dataStorage = new DataStorage(this.getContext());

        String targa = getArguments().getString("targa");
        String data = getArguments().getString("data");

        if (managerData.getCurrentUser() != null) {
            getSessionDetails(targa, data);
        } else {
            getSessionDetailsFromFile(data);
        }

        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public void onStart() {
        super.onStart();
        final Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            Objects.requireNonNull(dialog.getWindow()).setLayout(width, height);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle state) {
        super.onCreateView(inflater, parent, state);

        rootView = inflater.inflate(R.layout.dialog_statistics, parent, false);

        Toolbar toolbar = rootView.findViewById(R.id.toolbar_statistic);

        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);

        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_baseline_clear_24px));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                loadMultipleLineGraph();
                loadTipoStrada();
                loadMultipleGraph2();
            }
        }, 250);

        return rootView;
    }


    private void getSessionDetailsFromFile(String data) {
        Session session = null;
        for (Session val : dataStorage.loadSession()) {
            if (val.getCurrentData().equals(data)) {
                session = val;
                break;
            }
        }
        for (OBDStats value : Objects.requireNonNull(session).getOBDSession()) {
            gpsLongitude.add(value.getGPSLongitude());
            gpsLatitude.add(value.getGPSLatitude());
            obdSpeed.add((int) value.getOBDSpeed());
            engineRPM.add(value.getEngineRPM());
            engineLoad.add(value.getEngineLoad());
            fuelFlowRate.add(value.getFuelFlowRate());
            acceleratorPedalRelativePosition.add(value.getAcceleratorPedalRelativePosition());
            rettilineo.add(value.isRettilineo());
            pendenza.add(value.getPendenzaStrada());
            tipoStrada.add(value.getTipoStrada());
        }
    }


    private void getSessionDetails(String targa, String data) {
        progressDialog = new ProgressDialog(this.getContext());
        progressDialog.setMessage(this.getContext().getResources().getString(R.string.loading_car));
        progressDialog.setTitle(R.string.loading_car_title);
        progressDialog.setMax(50);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();
        // Read from the database
        DatabaseReference myRef = managerData.getRefSessionFromData(targa, data);

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                if (dataSnapshot.exists()) {

                    for (DataSnapshot obdSessionSnap : dataSnapshot.getChildren()) {

                        gpsLongitude.add(
                                Double.valueOf(obdSessionSnap.child("gpslongitude").getValue().toString())
                        );

                        gpsLatitude.add(
                                Double.valueOf(obdSessionSnap.child("gpslatitude").getValue().toString())
                        );

                        obdSpeed.add(
                                Integer.valueOf(obdSessionSnap.child("obdspeed").getValue().toString())
                        );

                        engineRPM.add(
                                Integer.valueOf(obdSessionSnap.child("engineRPM").getValue().toString())
                        );

                        engineLoad.add(
                                Double.valueOf(obdSessionSnap.child("engineLoad").getValue().toString())
                        );

                        fuelFlowRate.add(
                                Double.valueOf(obdSessionSnap.child("fuelFlowRate").getValue().toString())
                        );

                        acceleratorPedalRelativePosition.add(
                                Double.valueOf(obdSessionSnap.child("acceleratorPedalRelativePosition").getValue().toString())
                        );

                        rettilineo.add(
                                Boolean.valueOf(obdSessionSnap.child("rettilineo").getValue().toString())
                        );

                        pendenza.add(
                                Integer.valueOf(obdSessionSnap.child("pendenzaStrada").getValue().toString())
                        );

                        tipoStrada.add(
                                obdSessionSnap.child("tipoStrada").getValue().toString()
                        );

                    }
                }
                progressDialog.dismiss();

            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException());
                progressDialog.dismiss();
            }
        });


    }

    private void loadMultipleLineGraph() {
        LineChart chartMultipleLine = rootView.findViewById(R.id.chart_rilevazioni);
        int x = 0;

        ArrayList<Entry> yVal2 = new ArrayList<>();
        for (Integer i : engineRPM) {
            yVal2.add(new Entry(x++, i / 100));
        }

        x = 0;
        ArrayList<Entry> yVal3 = new ArrayList<>();
        for (Double i : engineLoad) {
            yVal3.add(new Entry(x++, Float.valueOf(i.toString()) / 10));
        }

        x = 0;
        ArrayList<Entry> yVal4 = new ArrayList<>();
        for (Double i : fuelFlowRate) {
            yVal4.add(new Entry(x++, Float.valueOf(i.toString())));
        }

        LineDataSet set2 = new LineDataSet(yVal2, "engineRPM");
        LineDataSet set3 = new LineDataSet(yVal3, "engineLoad");
        LineDataSet set4 = new LineDataSet(yVal4, "fuelFlowRate");

        set2.setDrawCircles(false);
        set3.setDrawCircles(false);
        set4.setDrawCircles(false);

        set2.setColor(JOYFUL_COLORS[1]);
        set3.setColor(JOYFUL_COLORS[4]);
        set4.setColor(PASTEL_COLORS[0]);

        LineData data = new LineData();
        data.addDataSet(set2);
        data.addDataSet(set4);
        data.addDataSet(set3);

//        chartMultipleLine.getLegend().setEnabled(false);
        Legend l = chartMultipleLine.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);

        chartMultipleLine.getAxisRight().setEnabled(false);

        XAxis xl = chartMultipleLine.getXAxis();
        xl.setPosition(XAxis.XAxisPosition.BOTTOM);
        xl.enableGridDashedLine(10f, 10f, 0f);

        chartMultipleLine.setData(data);

        chartMultipleLine.setTouchEnabled(true);

        chartMultipleLine.getData().setHighlightEnabled(true);
        chartMultipleLine.invalidate();

        chartMultipleLine.animateX(1000);
        chartMultipleLine.getDescription().setEnabled(false);
    }


    private void loadTipoStrada() {

        PieChart chartTipoStrada = rootView.findViewById(R.id.chart_strada);
        int stradaUrbana = 0;
        int stradaExtraurbanaPrinc = 0;
        int autostrada = 0;
        int stradaExtraurbanaSec = 0;

        for (String tipo : tipoStrada) {
            switch (tipo) {

                case "StradaUrbana":
                    stradaUrbana++;
                    break;

                case "StradaExtraurbanaSec":
                    stradaExtraurbanaSec++;
                    break;

                case "StradaExtraurbanaPrinc":
                    stradaExtraurbanaPrinc++;
                    break;

                case "Autostrada":
                    autostrada++;
                    break;

                default:
                    break;

            }

        }

        List<PieEntry> entries = new ArrayList<>();
        if (stradaUrbana != 0)
            entries.add(new PieEntry(stradaUrbana, "Urbana"));
        if (stradaExtraurbanaSec != 0)
            entries.add(new PieEntry(stradaExtraurbanaSec, "Extraurbana Secondaria"));
        if (stradaExtraurbanaPrinc != 0)
            entries.add(new PieEntry(stradaExtraurbanaPrinc, "Extraurbana Principale"));
        if (autostrada != 0)
            entries.add(new PieEntry(autostrada, "Autostrada"));

        PieDataSet dataset = new PieDataSet(entries, "");

        dataset.setColors(COLORFUL_COLORS);
        dataset.setDrawIcons(false);

        dataset.setSliceSpace(1f);
        dataset.setIconsOffset(new MPPointF(0, 40));
        dataset.setSelectionShift(5f);

        PieData data = new PieData();
        data.setDataSet(dataset);

        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(11f);

        Legend l = chartTipoStrada.getLegend();

        l.setOrientation(Legend.LegendOrientation.VERTICAL);
        l.setPosition(Legend.LegendPosition.LEFT_OF_CHART);
        l.setDrawInside(false);
        l.setXEntrySpace(7f);
        l.setYEntrySpace(0f);
        l.setYOffset(0f);

        chartTipoStrada.setEntryLabelColor(Color.BLACK);
        chartTipoStrada.setEntryLabelTextSize(12f);
        chartTipoStrada.setUsePercentValues(true);
        chartTipoStrada.getDescription().setEnabled(false);
        chartTipoStrada.animateY(2000);
        chartTipoStrada.setData(data);

    }

    private void loadMultipleGraph2() {
        LineChart lineChartGraph = rootView.findViewById(R.id.chart_press_velocita);

        int x = 0;
        ArrayList<Entry> yVal = new ArrayList<>();
        for (int i : obdSpeed) {
            yVal.add(new Entry(x++, i));
        }

        x = -1;
        ArrayList<Integer> stopGraph = new ArrayList<>();
        boolean isGreen = true;

        for (String tipo : tipoStrada) {
            int i;
            x++;
            switch (tipo) {
                case "StradaUrbana":
                    i = obdSpeed.get(x);
                    if ((i <= 40) != isGreen) {
                        isGreen = !isGreen;
                        stopGraph.add(x);
                    }
                    break;

                case "StradaExtraurbanaSec":
                    i = obdSpeed.get(x);
                    if ((i <= 90) != isGreen) {
                        isGreen = !isGreen;
                        stopGraph.add(x);
                    }
                    break;

                case "StradaExtraurbanaPrinc":
                    i = obdSpeed.get(x);
                    if ((i <= 110) != isGreen) {
                        isGreen = !isGreen;
                        stopGraph.add(x);
                    }
                    break;

                case "Autostrada":
                    i = obdSpeed.get(x);
                    if ((i <= 130) != isGreen) {
                        isGreen = !isGreen;
                        stopGraph.add(x);
                    }
                    break;
                default:
                    break;
            }
        }

        Log.d("INFR", stopGraph.toString() );

        LineDataSet set1 = new LineDataSet(yVal, "obdSpeed");
        set1.setDrawCircles(false);
        set1.setColor(Color.rgb(036,231,017));
        LineData data = new LineData();
        data.addDataSet(set1);

        for (int k = 0; k < stopGraph.size(); k = k + 2) {
            List<Entry> limiteSuperato;
            if (k == stopGraph.size() - 1) {
                limiteSuperato = yVal.subList(stopGraph.get(k), yVal.size() - 1);
            } else
                limiteSuperato = yVal.subList(stopGraph.get(k), stopGraph.get(k + 1));

            LineDataSet redLineSet = new LineDataSet(limiteSuperato, "limiteSuperato");
            redLineSet.setColor(Color.rgb(250, 0, 0));
            redLineSet.setCircleColors(redLineSet.getColors());
            redLineSet.setDrawValues(false);
            data.addDataSet(redLineSet);
        }

        x = 0;
        ArrayList<Entry> entry = new ArrayList<>();
        for (Double i : acceleratorPedalRelativePosition) {
            entry.add(new BarEntry(x++, Float.valueOf(i.toString())));
        }

        LineDataSet set2 = new LineDataSet(entry, "acceleratorPedalRelativePosition");
        set2.setColor(PASTEL_COLORS[2]);
        set2.setDrawCircles(false);
        data.addDataSet(set2);

        lineChartGraph.getAxisRight().setEnabled(false);

        lineChartGraph.getAxisLeft().setAxisMinimum(0);

        XAxis xl = lineChartGraph.getXAxis();
        xl.setPosition(XAxis.XAxisPosition.BOTTOM);
        xl.enableGridDashedLine(10f, 10f, 0f);
        xl.setAxisMinimum(-1);
        lineChartGraph.setData(data);

        lineChartGraph.setTouchEnabled(true);

        lineChartGraph.getData().setHighlightEnabled(true);
        lineChartGraph.invalidate();

        lineChartGraph.animateX(1000);
        lineChartGraph.getDescription().setEnabled(false);

        //        lineChartGraph.getLegend().setEnabled(false);
        Legend l = lineChartGraph.getLegend();
        LegendEntry[] legendEntries = l.getEntries();
        if (legendEntries.length > 2) {
            l.setCustom(new LegendEntry[]{legendEntries[0], legendEntries[1], legendEntries[legendEntries.length - 1]});

        }

    }
}
