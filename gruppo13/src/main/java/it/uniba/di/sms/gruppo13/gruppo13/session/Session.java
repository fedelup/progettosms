package it.uniba.di.sms.gruppo13.gruppo13.session;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;

import it.uniba.di.sms.gruppo13.gruppo13.data.obd.OBDStats;

public class Session implements Serializable {

    private String currentData;
    private ArrayList<OBDStats> OBDSession;

    public Session() {
        currentData = Calendar.getInstance().getTime().toString();
        OBDSession = new ArrayList<>();
    }

    public void addOBDStat(OBDStats stats) {
        OBDSession.add(stats);
    }

    public ArrayList<OBDStats> getOBDSession() {
        return OBDSession;
    }

    public String getCurrentData() {
        return currentData;
    }

    public void setCurrentData(String currentData) {
        this.currentData = currentData;
    }

    public void setOBDSession(ArrayList<OBDStats> OBDSession) {
        this.OBDSession = OBDSession;
    }

    public int getNumberRilevation() {
        return OBDSession.size();
    }

    public int getInfraction() {
        int infrazioniNewSession = 0;
        for (OBDStats stat : OBDSession) {
            String strada = stat.getTipoStrada();
            switch (strada) {
                case "StradaUrbana":
                    if (stat.getOBDSpeed() > 40)
                        infrazioniNewSession++;
                    break;
                case "StradaExtraurbanaSec":
                    if (stat.getOBDSpeed() > 90)
                        infrazioniNewSession++;
                    break;
                case "StradaExtraurbanaPrinc":
                    if (stat.getOBDSpeed() > 110)
                        infrazioniNewSession++;
                    break;
                case "Autostrada":
                    if (stat.getOBDSpeed() > 130)
                        infrazioniNewSession++;
                    break;
                default:
                    break;
            }
        }

        return infrazioniNewSession;
    }

    @Override
    public String toString() {
        String temp = "";
        temp += "Data: " + this.getCurrentData();
        temp += OBDSession.toString();
        return temp;
    }
}
