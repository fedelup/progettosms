package it.uniba.di.sms.gruppo13.gruppo13.car;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;

import java.util.Objects;

import it.uniba.di.sms.gruppo13.gruppo13.R;
import it.uniba.di.sms.gruppo13.gruppo13.data.ManagerData;
import it.uniba.di.sms.gruppo13.gruppo13.data.PreferenceManager;

public class DialogNewCar extends DialogFragment {

    public static final String TAG = "DialogNewCar";
    private EditText model;
    private EditText targa;
    private EditText nickname;
    private Boolean isNicknameClicked = false;
    private AutoCompleteTextView brand;

    private ManagerData managerData;

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (PreferenceManager.getInstance(getContext()).isDemoUser())
            getActivity().recreate();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle);
        managerData = new ManagerData(this.getContext());
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public void onStart() {
        super.onStart();
        final Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            Objects.requireNonNull(dialog.getWindow()).setLayout(width, height);
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle state) {
        super.onCreateView(inflater, parent, state);

        View rootView = inflater.inflate(R.layout.dialog_layout, parent, false);
        Toolbar toolbar = rootView.findViewById(R.id.toolbar_dialog);

        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);

        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_baseline_clear_24px));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        brand = rootView.findViewById(R.id.autoCompleteTextview_marca);

        String[] suggestions = getResources().getStringArray(R.array.car_list);
        ArrayAdapter<String> adapter =
                new ArrayAdapter<String>(rootView.getContext(),
                        android.R.layout.simple_expandable_list_item_1, suggestions);

        brand.setAdapter(adapter);
        brand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                brand.showDropDown();
            }
        });

        model = rootView.findViewById(R.id.modello);
        model.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                brand.dismissDropDown();
            }
        });

        targa = rootView.findViewById(R.id.targa);
        targa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                brand.dismissDropDown();
            }
        });

        targa.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (!isNicknameClicked) {
                    nickname.setText(targa.getText().toString());
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {
                //Nothing to do here

            }

        });


        nickname = rootView.findViewById(R.id.nickname);
        nickname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                brand.dismissDropDown();
                isNicknameClicked = true;
            }
        });

        Button add_car = (Button) rootView.findViewById(R.id.save_car);
        add_car.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                brand.setError(null);
                model.setError(null);
                targa.setError(null);

                String string_targa = targa.getText().toString().trim();
                String string_brand = brand.getText().toString().trim();
                String string_model = model.getText().toString().trim();
                String string_nickname = nickname.getText().toString().trim();

                if (string_brand.length() == 0) {
                    CharSequence cs = getResources().getString(R.string.inserire_un_brand_valido);
                    DialogNewCar.this.getBrand().setError(cs);
                } else if (string_model.length() == 0) {
                    CharSequence cs = getResources().getString(R.string.inserire_un_modello_valido);
                    DialogNewCar.this.getModel().setError(cs);
                } else if (string_targa.length() == 0) {
                    CharSequence cs = getResources().getString(R.string.inserire_una_targa_valida);
                    DialogNewCar.this.getTarga().setError(cs);
                } else {
                    saveCar(string_targa, string_brand, string_model, string_nickname);
                    dismiss();
                }
            }
        });

        return rootView;
    }


    public void saveCar(String targa, String brand, String model, String nickname) {
        managerData.postCar(new Car(targa, brand, model, nickname));
    }

    public EditText getTarga() {
        return targa;
    }

    public AutoCompleteTextView getBrand() {
        return brand;
    }

    public EditText getModel() {
        return model;
    }

    public EditText getNickname() {
        return nickname;
    }
}