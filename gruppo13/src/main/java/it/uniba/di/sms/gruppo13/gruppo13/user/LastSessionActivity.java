package it.uniba.di.sms.gruppo13.gruppo13.user;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import it.uniba.di.sms.gruppo13.gruppo13.R;
import it.uniba.di.sms.gruppo13.gruppo13.data.DataStorage;
import it.uniba.di.sms.gruppo13.gruppo13.data.ManagerData;
import it.uniba.di.sms.gruppo13.gruppo13.session.adapters.*;

import java.util.ArrayList;

import it.uniba.di.sms.gruppo13.gruppo13.session.Session;

import static android.content.ContentValues.TAG;

public class LastSessionActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private Context context;
    private ProgressDialog progressDialog;
    private ArrayList<ParentSessionItem> arrayCarTarghe = new ArrayList<>();


    private ManagerData managerData;
    private RecyclerDataAdapter recyclerDataAdapter;
    private LayoutAnimationController animation;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        managerData = new ManagerData(this);
        int resId = R.anim.layout_animation_slide_right;
        setContentView(R.layout.activity_last_session);
        context = LastSessionActivity.this;

        animation = AnimationUtils.loadLayoutAnimation(context, resId);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerViewLastSession);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerDataAdapter = new RecyclerDataAdapter(arrayCarTarghe);
        recyclerView.setAdapter(recyclerDataAdapter);
        Button back = (Button) findViewById(R.id.toolbar_btn_back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        if (managerData.getCurrentUser() != null) {
            getDataFromFB();
        } else getDataFromFile();

        loadAnim();


    }

    private void loadAnim() {
        recyclerView.setLayoutAnimation(animation);
        recyclerView.scheduleLayoutAnimation();
        recyclerView.startLayoutAnimation();
    }


    @Override
    public void onBackPressed() {
        this.finish();
    }

    public void getDataFromFile() {
        DataStorage dataStorage = new DataStorage(context);
        ArrayList<Session> sessions = dataStorage.loadSession();

        ArrayList<ChildSessionItem> childDataItems;
        ParentSessionItem parentSessionItem;
        ChildSessionItem childDataItem;

        parentSessionItem = new ParentSessionItem();
        parentSessionItem.setParentName(dataStorage.loadCar().getTarga());
        childDataItems = new ArrayList<>();

        for (int i = 0; i < sessions.size(); i++) {
            childDataItem = new ChildSessionItem();
            childDataItem.setChildName(sessions.get(i).getCurrentData());
            childDataItems.add(childDataItem);
            Log.d("ASDPEL", childDataItems.toString());
        }

        parentSessionItem.setChildSessionItems(childDataItems);
        arrayCarTarghe.add(parentSessionItem);
        recyclerDataAdapter.notifyItemRangeChanged(0, arrayCarTarghe.size());

    }


    private void getDataFromFB() {
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(context.getResources().getString(R.string.loading_car));
        progressDialog.setTitle(R.string.loading_car_title);
        progressDialog.setMax(50);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();

        DatabaseReference myRef = managerData.getRefDbCar();
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {

                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {

                        ArrayList<ChildSessionItem> childDataItems;
                        ParentSessionItem parentSessionItem;
                        ChildSessionItem childDataItem;

                        parentSessionItem = new ParentSessionItem();
                        parentSessionItem.setParentName(snapshot.getKey());
                        childDataItems = new ArrayList<>();

                        for (DataSnapshot snapshotSession : snapshot.child("session").getChildren()) {

                            childDataItem = new ChildSessionItem();
                            childDataItem.setChildName(snapshotSession.getKey());
                            childDataItems.add(childDataItem);
                        }
                        if (childDataItems.isEmpty()) {
                            childDataItem = new ChildSessionItem();
                            childDataItem.setChildName(getString(R.string.nessuna_sessione_eseguita));
                            childDataItems.add(childDataItem);
                        }
                        parentSessionItem.setChildSessionItems(childDataItems);
                        arrayCarTarghe.add(parentSessionItem);
                        recyclerDataAdapter.notifyItemRangeChanged(0, arrayCarTarghe.size());

                    }
                }
                progressDialog.dismiss();

            }

            @Override
            public void onCancelled(DatabaseError error) {
                Log.w(TAG, getString(R.string.errore_lettura_valori), error.toException());
                progressDialog.dismiss();
            }
        });

        recyclerDataAdapter.notifyDataSetChanged();
    }

    private class RecyclerDataAdapter extends RecyclerView.Adapter<RecyclerDataAdapter.MyViewHolder> {
        private ArrayList<ParentSessionItem> parentSessionItems;

        RecyclerDataAdapter(ArrayList<ParentSessionItem> parentSessionItems) {
            this.parentSessionItems = parentSessionItems;
        }

        @Override
        public RecyclerDataAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_parent_child_listing, parent, false);
            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(RecyclerDataAdapter.MyViewHolder holder, int pos) {
            ParentSessionItem parentSessionItem = parentSessionItems.get(pos);
            holder.textView_parentName.setText(parentSessionItem.getParentName());

            int noOfChildTextViews = holder.linearLayout_childItems.getChildCount();
            int noOfChild = parentSessionItem.getChildSessionItems().size();
            if (noOfChild < noOfChildTextViews) {
                for (int index = noOfChild; index < noOfChildTextViews; index++) {
                    TextView currentTextView = (TextView) holder.linearLayout_childItems.getChildAt(index);
                    currentTextView.setVisibility(View.GONE);
                }
            }

            for (int textViewIndex = 0; textViewIndex < noOfChild; textViewIndex++) {
                TextView currentTextView = (TextView) holder.linearLayout_childItems.getChildAt(textViewIndex);
                currentTextView.setText(parentSessionItem.getChildSessionItems().get(textViewIndex).getChildName());
            }


        }


        @Override
        public int getItemCount() {
            return parentSessionItems.size();
        }


        class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
            private Context context;
            private TextView textView_parentName;
            private LinearLayout linearLayout_childItems;

            MyViewHolder(View itemView) {
                super(itemView);
                context = itemView.getContext();
                textView_parentName = itemView.findViewById(R.id.tv_parentName);
                linearLayout_childItems = itemView.findViewById(R.id.ll_child_items);
                linearLayout_childItems.setVisibility(View.GONE);
                int intMaxNoOfChild = 0;
                for (int index = 0; index < parentSessionItems.size(); index++) {
                    int intMaxSizeTemp = parentSessionItems.get(index).getChildSessionItems().size();
                    if (intMaxSizeTemp > intMaxNoOfChild) intMaxNoOfChild = intMaxSizeTemp;
                }
                for (int indexView = 0; indexView < intMaxNoOfChild; indexView++) {
                    TextView textView = new TextView(context);
                    textView.setId(indexView);
                    textView.setPadding(20, 20, 0, 20);
                    textView.setGravity(Gravity.START);
                    textView.setBackground(ContextCompat.getDrawable(context, R.drawable.background_sub_module_text));
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    textView.setOnClickListener(this);
                    linearLayout_childItems.addView(textView, layoutParams);
                }
                textView_parentName.setOnClickListener(this);
            }

            @Override
            public void onClick(View view) {
                if (view.getId() == R.id.tv_parentName) {
                    if (linearLayout_childItems.getVisibility() == View.VISIBLE) {
                        linearLayout_childItems.setVisibility(View.GONE);
                    } else {
                        linearLayout_childItems.setVisibility(View.VISIBLE);
                        for (int i = 0; i < linearLayout_childItems.getChildCount(); i++) {
                            setAnimationVisible(linearLayout_childItems.getChildAt(i), view.getId());
                        }
                    }
                } else {
                    TextView textViewClicked = (TextView) view;

                    Bundle args = new Bundle();
                    args.putString("targa", textView_parentName.getText().toString());
                    args.putString("data", ((TextView) view).getText().toString());

                    FragmentManager fragmentManager = getSupportFragmentManager();
                    DialogShowSession newDialogShowSession = new DialogShowSession();
                    FragmentTransaction transaction = fragmentManager.beginTransaction();
                    transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                    transaction.add(R.id.content_last, newDialogShowSession).addToBackStack(null).commit();

                    newDialogShowSession.setArguments(args);

                }
            }

            private void setAnimationVisible(View view, int pos) {
                Animation anim = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
                view.startAnimation(anim);
            }


        }
    }

}
