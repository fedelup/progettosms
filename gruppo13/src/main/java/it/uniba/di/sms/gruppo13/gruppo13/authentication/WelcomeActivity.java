package it.uniba.di.sms.gruppo13.gruppo13.authentication;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Locale;

import it.uniba.di.sms.gruppo13.gruppo13.R;
import it.uniba.di.sms.gruppo13.gruppo13.data.PreferenceManager;
import it.uniba.di.sms.gruppo13.gruppo13.user.MainActivity;
import it.uniba.di.sms.gruppo13.gruppo13.user.SettingsFragment;

public class WelcomeActivity extends Fragment {

    private PreferenceManager preferenceManager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View mView = inflater.inflate(R.layout.activity_welcome, container, false);
        preferenceManager = PreferenceManager.getInstance(getContext());
        loadLanguage();

        LinearLayout linearLayout = mView.findViewById(R.id.linearlayout);
        TextView appName = mView.findViewById(R.id.app_name);

        Animation animDownTop = AnimationUtils.loadAnimation(getActivity().getApplicationContext(),
                R.anim.translate_down_up);
        Animation animTopDown = AnimationUtils.loadAnimation(getActivity().getApplicationContext(),
                R.anim.translate_up_down);

        linearLayout.setAnimation(animDownTop);
        appName.setAnimation(animTopDown);


        Button loginButton = mView.findViewById(R.id.login);
        Button signInButton = mView.findViewById(R.id.signIn);
        Button demoButton = mView.findViewById(R.id.demo);

        Typeface typeface_titleApp = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Quantify.ttf");
        appName.setTypeface(typeface_titleApp);

        Typeface buttons = Typeface.createFromAsset(getActivity().getAssets(), "fonts/roboto_medium.ttf");
        loginButton.setTypeface(buttons);
        signInButton.setTypeface(buttons);
        demoButton.setTypeface(buttons);


        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), LoginActivity.class));
            }
        });

        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), RegistrationActivity.class));
            }
        });

        demoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                preferenceManager.setDemoUser(true);
                startActivity(new Intent(getActivity(), MainActivity.class));
                getActivity().finish();
            }
        });

        return mView;
    }

    public void loadLanguage() {
        SharedPreferences sharedPref = getContext().getSharedPreferences(SettingsFragment.KEY_PREF_LANGUAGE, Activity.MODE_PRIVATE);
        String actual_language = sharedPref.getString(SettingsFragment.KEY_PREF_LANGUAGE, "");
        Locale locale;
        switch (actual_language) {
            case "Italiano":
                locale = new Locale("it");
                break;
            case "Italian":
                locale = new Locale("it");
                break;
            case "Inglese":
                locale = new Locale("en");
                break;
            case "English":
                locale = new Locale("en");
                break;
            default:
                locale = new Locale(Locale.getDefault().getLanguage());
                break;
        }
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        this.getResources().updateConfiguration(config, null);
    }

}
