package it.uniba.di.sms.gruppo13.gruppo13.user;


import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseUser;

import java.util.Objects;

import it.uniba.di.sms.gruppo13.gruppo13.R;
import it.uniba.di.sms.gruppo13.gruppo13.car.GarageActivity;
import it.uniba.di.sms.gruppo13.gruppo13.data.ManagerData;
import it.uniba.di.sms.gruppo13.gruppo13.data.PreferenceManager;
import it.uniba.di.sms.gruppo13.gruppo13.introapp.SplashScreenActivity;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    public static final String TAG = "MainActivity";
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle toggle;

    private PreferenceManager preferenceManager;
    private ManagerData managerData;
    private int doubleClickBack = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        preferenceManager = PreferenceManager.getInstance(this);
        preferenceManager.loadLanguage();
        setContentView(R.layout.activity_user_session);
        overridePendingTransition(R.anim.entry2, R.anim.exit2);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout_userSession);
        toggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.open, R.string.close);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        TextView headerUsername = navigationView.getHeaderView(0).findViewById(R.id.nav_header_username);
        TextView headerEmail = navigationView.getHeaderView(0).findViewById(R.id.nav_header_email);
        drawerLayout.addDrawerListener(toggle);

        managerData = new ManagerData(this);

        if (preferenceManager.isLogAccount()) {
            FirebaseUser currentUser = managerData.getCurrentUser();
            headerUsername.setText(currentUser.getDisplayName());
            headerEmail.setText(currentUser.getEmail());
        } else {
            Objects.requireNonNull(getSupportActionBar()).setTitle(getString(R.string.actionbar_title));
            headerUsername.setText("");
            headerEmail.setText(getString(R.string.actionbar_message));
        }

        navigationView.setNavigationItemSelectedListener(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toggle.syncState();

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                    new HomeFragment()).commit();
            navigationView.setCheckedItem(R.id.dashboard);
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
    }


    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
            doubleClickBack = 0;
        } else {
            doubleClickBack += 1;
            if (doubleClickBack == 2)
                super.onBackPressed();
        }


    }


    @Override
    public boolean onOptionsItemSelected(MenuItem menu) {
        if (toggle.onOptionsItemSelected(menu)) {
            return true;
        }
        return super.onOptionsItemSelected(menu);
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {

            case R.id.dashboard:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new HomeFragment(), TAG).commit();
                break;
            case R.id.my_garage:
                startActivity(new Intent(MainActivity.this, GarageActivity.class));
                break;
            case R.id.language:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container, new SettingsFragment())
                        .commit();
                break;
            case R.id.information:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new InformationFragment()).commit();
                break;
            case R.id.logout:
                if (preferenceManager.isLogAccount()) {
                    managerData.getInstanceLogin().signOut();
                }
                preferenceManager.setDemoUser(false);
                preferenceManager.setAccount(false);
                Intent intent = new Intent(MainActivity.this, SplashScreenActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
                break;
            default:
                break;
        }

        drawerLayout.closeDrawer(GravityCompat.START);

        return true;
    }
}
