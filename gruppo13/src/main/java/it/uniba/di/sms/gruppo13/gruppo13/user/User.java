package it.uniba.di.sms.gruppo13.gruppo13.user;

import java.util.Date;

public class User {
    private String uID;
    private String fullName;
    private String sesso;
    private Date dataNascita;

    public User(String uID, String fullName, String sesso, Date dataNascita) {
        this.uID = uID;
        this.fullName = fullName;
        this.sesso = sesso;
        this.dataNascita = dataNascita;
    }

    public String getuID() {
        return uID;
    }

    public String getFullName() {
        return fullName;
    }

    public String getSesso() {
        return sesso;
    }

    public Date getDataNascita() {
        return dataNascita;
    }
}

