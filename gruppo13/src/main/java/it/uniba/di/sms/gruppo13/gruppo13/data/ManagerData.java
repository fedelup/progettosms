package it.uniba.di.sms.gruppo13.gruppo13.data;

import android.content.Context;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import it.uniba.di.sms.gruppo13.gruppo13.user.User;
import it.uniba.di.sms.gruppo13.gruppo13.car.Car;
import it.uniba.di.sms.gruppo13.gruppo13.session.Session;

public class ManagerData extends FirebaseDataAccess {

    private List<String> dbUser = new ArrayList<>();
    private List<String> dbCar = new ArrayList<>();
    private List<String> dbSession = new ArrayList<>();
    private ArrayList<String> dbVal = new ArrayList<>();

    private Boolean loginUser;
    private DataStorage dataStorage;

    public ManagerData(Context context) {
            loginUser = PreferenceManager.getInstance(context).isLogAccount();
            if (loginUser) {
                dbUser.add("utenti");
                dbUser.add(Objects.requireNonNull(mAuth.getCurrentUser()).getUid());
                dbCar.addAll(dbUser);
                dbCar.add("car");
            } else
                dataStorage = new DataStorage(context);

    }

    public FirebaseUser getCurrentUser() {
        if (loginUser)
            return mAuth.getCurrentUser();
        else
            return null;
    }

    public FirebaseAuth getInstanceLogin() {
        if (loginUser)
            return mAuth;
        else
            return null;
    }

    public void postCar(Car car) {
        if (loginUser) {
            dbCar.add(car.getTarga());
            putData(dbCar, car);
            dbCar.remove(car.getTarga());
        } else {
            dataStorage.saveCar(car);
        }
    }

    public boolean deleteCar(String targa) {
        boolean b = false;
        if (!loginUser)
            b = dataStorage.deleteCarFile();
        else {
            dbCar.add(targa);
            b = deleteData(dbCar);
            dbCar.remove(targa);
        }
        return b;
    }


    public void postUser(User user) {
        dbUser.add("utenti");
        dbUser.add(Objects.requireNonNull(mAuth.getCurrentUser()).getUid());
        putData(dbUser, user);
        postValutation(0, 0);
    }

    public void postValutation(int rilevazioni, int infrazioni) {
        if (loginUser) {
            dbVal.clear();
            dbVal.addAll(dbUser);
            dbVal.add("valutazione");
            dbVal.add("nRilevazioni");
            putData(dbVal, rilevazioni);

            dbVal.clear();
            dbVal.addAll(dbUser);
            dbVal.add("valutazione");
            dbVal.add("totInfrazioni");
            putData(dbVal, infrazioni);
        } else {
            dataStorage.saveRilevazioni(rilevazioni);
            dataStorage.saveTotInfraction(infrazioni);
        }
    }


    public DatabaseReference getRefDbCar() {
        if (loginUser) {
            myRef = database.getReference(nomeDb);
            for (String child : dbCar) {
                myRef = myRef.child(child);
            }
            return myRef;
        } else {
            return null;
        }
    }

    public DatabaseReference getRefValutation() {
        if (loginUser) {
            dbVal.clear();
            dbVal.addAll(dbUser);
            dbVal.add("valutazione");

            myRef = database.getReference(nomeDb);
            for (String child : dbVal) {
                myRef = myRef.child(child);
            }
            return myRef;
        } else

        {
            return null;
        }

    }

    public boolean postSession(String targa, Session session) {
        if (loginUser) {
            dbSession.clear();
            dbSession.addAll(dbCar);
            dbSession.add(targa);
            dbSession.add("session");
            dbSession.add(session.getCurrentData());
            return putData(dbSession, session);
        } else {
            return dataStorage.saveSession(session);
        }
    }


    public DatabaseReference getRefSessionFromData(String targa, String data) {
        dbSession.clear();
        dbSession.addAll(dbCar);
        dbSession.add(targa);
        dbSession.add("session");
        dbSession.add(data);
        dbSession.add("obdsession");

        if (loginUser) {
            myRef = database.getReference(nomeDb);
            for (String child : dbSession) {
                myRef = myRef.child(child);
            }
            return myRef;
        } else {
            return null;
        }

    }

}
