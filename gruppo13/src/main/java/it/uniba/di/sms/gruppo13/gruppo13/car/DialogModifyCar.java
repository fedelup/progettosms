package it.uniba.di.sms.gruppo13.gruppo13.car;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import it.uniba.di.sms.gruppo13.gruppo13.R;
import it.uniba.di.sms.gruppo13.gruppo13.data.ManagerData;

public class DialogModifyCar extends DialogFragment implements View.OnClickListener {

    private TextInputEditText brand, model, targa, nickname;


    private Car car;
    private ManagerData managerData;

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup parent, Bundle state) {
        super.onCreateView(inflater, parent, state);

        View root = inflater.inflate(R.layout.modify_car, parent, false);

        managerData = new ManagerData(this.getContext());

        ImageView brandLogo = root.findViewById(R.id.brand_image);
        brand = root.findViewById(R.id.brand);
        model = root.findViewById(R.id.model);
        targa = root.findViewById(R.id.targa);
        nickname = root.findViewById(R.id.nickname);
        Button save = root.findViewById(R.id.save);
        Button cancel = root.findViewById(R.id.annul);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        save.setOnClickListener(this);

        Bundle mArgs = getArguments();
        car = mArgs.getParcelable("car");

        brandLogo.setImageResource(car.getThumbnail(getContext()));

        brand.setText(car.getBrand());
        model.setText(car.getModel());
        targa.setText(car.getTarga());
        nickname.setText(car.getNickname());
        return root;
    }


    @Override
    public void onClick(View view) {
        String brand = this.brand.getText().toString().trim();
        String model = this.model.getText().toString().trim();
        String targa = this.targa.getText().toString().trim();
        String nickname = this.nickname.getText().toString().trim();


        car = new Car(targa, brand, model, nickname);

        dismiss();
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        managerData.postCar(car);
    }
}
