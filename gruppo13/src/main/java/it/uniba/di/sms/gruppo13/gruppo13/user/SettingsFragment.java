package it.uniba.di.sms.gruppo13.gruppo13.user;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceFragmentCompat;

import java.util.Locale;

import it.uniba.di.sms.gruppo13.gruppo13.R;
import it.uniba.di.sms.gruppo13.gruppo13.data.PreferenceManager;

public class SettingsFragment extends PreferenceFragmentCompat implements SharedPreferences.OnSharedPreferenceChangeListener {

    public static final String TAG = "SettingFragment";
    public static final String KEY_PREF_LANGUAGE = "pref_language";

        @Override
        public void onCreatePreferences(Bundle bundle, String s) {
            addPreferencesFromResource(R.xml.preferences);
        }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        PreferenceManager preferenceManager = PreferenceManager.getInstance(this.getContext());
        SharedPreferences.Editor editor = preferenceManager.getEditor();

        if (getPreferenceManager().getSharedPreferences().getString(KEY_PREF_LANGUAGE,"").isEmpty()){
            editor.putString(KEY_PREF_LANGUAGE, Locale.getDefault().getLanguage());
        }

        Preference connectionPref = findPreference(KEY_PREF_LANGUAGE);
        connectionPref.setSummary(getPreferenceManager().getSharedPreferences().getString(KEY_PREF_LANGUAGE, ""));
    }

    @Override
        public void onResume() {
            super.onResume();
            getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);

        }

        @Override
        public void onPause() {
            super.onPause();
            getPreferenceManager().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
        }

        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
            if (key.equals(KEY_PREF_LANGUAGE)) {
                Preference connectionPref = findPreference(key);
                changeLanguagePref(sharedPreferences.getString(key, ""));
                connectionPref.setSummary(sharedPreferences.getString(key, ""));

            }
        }

        private void changeLanguagePref(String lang) {
            Locale locale = null;
            if ("Italiano".equals(lang)) {
                locale = new Locale("it");
            }  else if ("English".equals(lang)) {
                locale = new Locale("en");
            }
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            this.getResources().updateConfiguration(config, null);
            SharedPreferences preferences = this.getActivity().getSharedPreferences(KEY_PREF_LANGUAGE, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString(KEY_PREF_LANGUAGE, lang);
            editor.commit();
            getActivity().recreate();
        }

}
