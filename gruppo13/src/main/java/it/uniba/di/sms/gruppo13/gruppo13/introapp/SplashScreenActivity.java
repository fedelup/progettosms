package it.uniba.di.sms.gruppo13.gruppo13.introapp;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import java.util.Locale;

import it.uniba.di.sms.gruppo13.gruppo13.R;
import it.uniba.di.sms.gruppo13.gruppo13.authentication.WelcomeActivity;
import it.uniba.di.sms.gruppo13.gruppo13.data.PreferenceManager;
import it.uniba.di.sms.gruppo13.gruppo13.user.MainActivity;
import it.uniba.di.sms.gruppo13.gruppo13.user.SettingsFragment;

public class SplashScreenActivity extends AppCompatActivity{

    private PreferenceManager preferenceManager;

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString("WORKAROUND_FOR_BUG_19917_KEY", "WORKAROUND_FOR_BUG_19917_VALUE");
        super.onSaveInstanceState(outState);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        preferenceManager  = PreferenceManager.getInstance(this);
        Log.d("Demo", "" + preferenceManager.isDemoUser());
        Log.d("Log", "" + preferenceManager.isLogAccount());
        preferenceManager.loadLanguage();
        setContentView(R.layout.activity_splash_screen);


        int SPLASH_TIME_OUT = 1800;
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                changeView();
            }
        }, SPLASH_TIME_OUT);
    }

    public void changeView(){
        if(preferenceManager.isDemoUser() || preferenceManager.isLogAccount()) {
            startActivity(new Intent(SplashScreenActivity.this, MainActivity.class));
            finish();
        }else{
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.splashScreen, new WelcomeActivity())
                    .commit();
        }

    }


}
