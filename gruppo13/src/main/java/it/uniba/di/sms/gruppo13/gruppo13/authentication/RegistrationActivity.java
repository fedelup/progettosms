package it.uniba.di.sms.gruppo13.gruppo13.authentication;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

import it.uniba.di.sms.gruppo13.gruppo13.R;
import it.uniba.di.sms.gruppo13.gruppo13.car.Car;
import it.uniba.di.sms.gruppo13.gruppo13.data.DataStorage;
import it.uniba.di.sms.gruppo13.gruppo13.data.ManagerData;
import it.uniba.di.sms.gruppo13.gruppo13.data.PreferenceManager;
import it.uniba.di.sms.gruppo13.gruppo13.session.Session;
import it.uniba.di.sms.gruppo13.gruppo13.user.MainActivity;
import it.uniba.di.sms.gruppo13.gruppo13.user.User;

public class RegistrationActivity extends AppCompatActivity implements View.OnClickListener {
    private TextInputEditText email_ed;
    private TextInputEditText password_ed;
    private TextInputEditText fullname_ed;
    private TextView mDate;
    private DatePickerDialog.OnDateSetListener mDateSetListener;
    private RadioGroup genderGroup;
    private Button btnRegistration;
    private ProgressDialog progressDialog;

    private FirebaseAuth auth;
    private PreferenceManager preferenceManager;
    private DataStorage dataStorage;
    private ManagerData managerData;

    @Override
    protected void onPause() {
        super.onPause();
        progressDialog = null;
    }


    @Override
    protected void onResume() {
        super.onResume();
        progressDialog = new ProgressDialog(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        progressDialog = new ProgressDialog(this);

        auth = FirebaseAuth.getInstance();
        preferenceManager = PreferenceManager.getInstance(this);
        managerData = new ManagerData(this);

        btnRegistration = (Button) findViewById(R.id.submit_btn);
        email_ed = (TextInputEditText) findViewById(R.id.email_field);
        password_ed = (TextInputEditText) findViewById(R.id.password_field);
        fullname_ed = (TextInputEditText) findViewById(R.id.fullname_field);
        genderGroup = (RadioGroup) findViewById(R.id.radio_group);
        btnRegistration.setOnClickListener(this);
        mDate = (TextView) findViewById(R.id.dateofbirth_field);


        mDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(
                        RegistrationActivity.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        mDateSetListener,
                        year, month, day);
                Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });

        mDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                String sMonth = Integer.toString(month);
                String sDayOfMonth = Integer.toString(dayOfMonth);
                month += 1;
                if (month < 10)
                    sMonth = "0" + month;
                if (dayOfMonth < 10)
                    sDayOfMonth = "0" + dayOfMonth;
                String date = sDayOfMonth + "/" + sMonth + "/" + year;
                mDate.setText(date);
            }
        };
    }

    private void registerUser() {
        final String email_field = email_ed.getText().toString().trim();
        final String password_field = password_ed.getText().toString().trim();
        final String fullName = fullname_ed.getText().toString().trim();
        final Date finalDateBirth;
        final String gender = genderGroupToString();

        SimpleDateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy", Locale.ITALY);

        Date dateBirth = new Date();
        try {
            dateBirth = dateformat.parse(mDate.getText().toString().trim());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        finalDateBirth = dateBirth;


        if (TextUtils.isEmpty(fullName)) {
            Toast.makeText(this, R.string.nome_vuoto_exception, Toast.LENGTH_LONG).show();
            return;
        }

        if ("".equals(gender)) {
            Toast.makeText(this, R.string.genere_vuoto_exception, Toast.LENGTH_LONG).show();
            return;
        }

        if (TextUtils.isEmpty(email_field)) {
            Toast.makeText(this, R.string.email_vuota_exception, Toast.LENGTH_LONG).show();
            return;
        }

        if (TextUtils.isEmpty(password_field)) {
            Toast.makeText(this, R.string.password_vuota_exception, Toast.LENGTH_LONG).show();
            return;
        }


        progressDialog.setMessage(getString(R.string.dialog_registrazione_utente));
        progressDialog.show();

        registration(email_field, password_field, fullName, gender, finalDateBirth);
        logIn(email_field,password_field);

    }



    private String genderGroupToString() {
        String temp = "";
        if (genderGroup.getCheckedRadioButtonId() != -1) {
            RadioButton radioButton = (RadioButton) findViewById(genderGroup.getCheckedRadioButtonId());
            temp = radioButton.getText().toString().trim();
        }
        return temp;
    }


    @Override
    public void onClick(View v) {
        if (v == btnRegistration)
            registerUser();
    }


    private void registration(final String email_field, final String password_field,
                              final String fullName, final String gender, final Date finalDateBirth){
        final Context context = this;
        auth.createUserWithEmailAndPassword(email_field, password_field)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                            User uto = new User(Objects.requireNonNull(user).getUid(), fullName, gender, finalDateBirth);

                            managerData.postUser(uto);

                            UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                                    .setDisplayName(fullName).build();
                            Objects.requireNonNull(auth.getCurrentUser()).updateProfile(profileUpdates);

                            progressDialog.dismiss();
                            Toast.makeText(context, R.string.toast_registrazione_completata, Toast.LENGTH_SHORT).show();
                            preferenceManager.setEmail(email_field);
                            preferenceManager.setPassword(password_field);
                        } else {
                            progressDialog.dismiss();
                            Toast.makeText(context, Objects.requireNonNull(task.getException()).getMessage(), Toast.LENGTH_SHORT).show();
                            Log.d(getString(R.string.toast_registrazione_fallita), task.getException().getMessage());
                        }
                    }
                });

    }

    private void logIn(final String email, final String password) {
        progressDialog.setMessage(getResources().getString(R.string.login_));
        progressDialog.show();
        final Context context = this;
        //authenticate user
        auth.signInWithEmailAndPassword(email.trim(), password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            preferenceManager.setEmail(email);
                            preferenceManager.setPassword(password);
                            preferenceManager.setAccount(true);
                            Intent intent = new Intent(RegistrationActivity.this, MainActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);

                            if (localDataExist()){
                                managerData = new ManagerData(context);
                                dataStorage = new DataStorage(context);
                                saveOnFirebase();
                            }

                            startActivity(intent);
                            finish();
                        } else {
                            Toast.makeText(RegistrationActivity.this,
                                    Objects.requireNonNull(task.getException()).getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                });
        progressDialog.dismiss();
    }

    private boolean localDataExist(){
        dataStorage = new DataStorage(this);
        if (dataStorage.loadCar() != null || dataStorage.loadSession().size() > 0)
            return true;
        else
            return false;
    }

    private void saveOnFirebase() {
        Car c = dataStorage.loadCar();
        String targa = "LocalSession";
        if (c != null ){
            managerData.postCar(c);
            targa = c.getTarga();
        }
        for (Session s: dataStorage.loadSession()) {
            managerData.postSession(targa, s);
        }
        managerData.postValutation(dataStorage.loadRilevazioni(), dataStorage.loadTotInfraction());
        dataStorage.resetFile();
    }

}