package it.uniba.di.sms.gruppo13.gruppo13.car.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import it.uniba.di.sms.gruppo13.gruppo13.R;
import it.uniba.di.sms.gruppo13.gruppo13.car.Car;

public class DiscreteScrollViewAdapter extends RecyclerView.Adapter<DiscreteScrollViewAdapter.ViewHolder> {
    private Context context;
    private ArrayList<Car> arrayList;

    public DiscreteScrollViewAdapter(Context context, ArrayList<Car> cars) {
        this.context = context;
        this.arrayList = cars;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.custom_listview, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Glide.with(holder.itemView.getContext())
                .load(arrayList.get(position).getThumbnail(context))
                .into(holder.logoMacchina);
        holder.infoMacchina.setText(arrayList.get(position).getNickname());
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView infoMacchina;
        private ImageView logoMacchina;

        ViewHolder(View itemView) {
            super(itemView);
            infoMacchina = itemView.findViewById(R.id.textview_item);
            logoMacchina = itemView.findViewById(R.id.imageView_item);

        }
    }
}
