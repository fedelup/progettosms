package it.uniba.di.sms.gruppo13.gruppo13.data.obd;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import it.uniba.di.sms.gruppo13.gruppo13.session.Session;

public class ParseJSON {

    private String json;

    public ParseJSON(String json) {
        this.json = json;
    }

    public Session CreateSession() {
        Session sessionOBD = new Session();
        ArrayList<String> state = new ArrayList<>();

        Pattern pattern = Pattern.compile("\\{(.*?)\\}");
        Matcher matcher = pattern.matcher(json.replaceAll(",", "."));
        while (matcher.find()) {
            String session = matcher.group(1);
            //Log.d(String.valueOf(i), session);
            Pattern patternSession = Pattern.compile(":\"(.*?)\"");
            Matcher matcherSession = patternSession.matcher(session);
            while (matcherSession.find()) {
                String attribute = matcherSession.group(1);
                state.add(attribute);
            }
            OBDStats obdStats = createStato(state);
            sessionOBD.addOBDStat(obdStats);
            //managerData.postSession(targa, data, i, obdStats);
            state.clear();
        }
        return sessionOBD;
    }

    private OBDStats createStato(ArrayList<String> state) {
        return new OBDStats(state.get(0), state.get(1), state.get(2), state.get(3),
                state.get(4), state.get(5), state.get(6), state.get(7), state.get(8), state.get(9));
    }


}
