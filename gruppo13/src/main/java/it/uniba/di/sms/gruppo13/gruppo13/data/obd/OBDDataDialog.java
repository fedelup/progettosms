package it.uniba.di.sms.gruppo13.gruppo13.data.obd;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;

import java.nio.charset.Charset;
import java.util.Objects;

import it.uniba.di.sms.gruppo13.gruppo13.R;


public class OBDDataDialog extends DialogFragment {

    private Button buttonRemoveConnection;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public void onStart() {
        super.onStart();
        final Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            Objects.requireNonNull(dialog.getWindow()).setLayout(width, height);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle state) {
        super.onCreateView(inflater, parent, state);

        View rootView = inflater.inflate(R.layout.activity_obd_data, parent, false);

        buttonRemoveConnection = rootView.findViewById(R.id.btn_remove_connection);

        buttonRemoveConnection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OBDConnectionActivity.mBluetoothConnection.write("DROPTHESERVICE".getBytes(Charset.defaultCharset()));
                dismiss();  
            }
        });


        return rootView;

    }

}
