package it.uniba.di.sms.gruppo13.gruppo13.data;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;

import java.util.Locale;

import it.uniba.di.sms.gruppo13.gruppo13.user.SettingsFragment;

public class PreferenceManager {

    @SuppressLint("StaticFieldLeak")
    private static PreferenceManager prefManager;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private Context context;

    // shared pref mode
    private int PRIVATE_MODE = 0;

    // Shared preferences file name
    private static final String PREF_NAME = "intro_slider-welcome";
    private static final String IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch";
    private static final String KEY_EMAIL = "email";
    private static final String KEY_PASS = "password";
    private static final String LOG_ACCOUNT = "log_account";
    private static final String DEMO_USER = "demoOrNot";


    private PreferenceManager(Context context) {
        this.context = context;
        pref = this.context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
        editor.apply();
    }

    public SharedPreferences.Editor getEditor() {
        return editor;
    }


    public static PreferenceManager getInstance(Context context) {
        if (prefManager == null) {
            prefManager = new PreferenceManager(context);
        }
        return prefManager;
    }

    public boolean setEmail(String email) {
        editor.putString(KEY_EMAIL, email);
        return editor.commit();
    }

    public String getEmail() {
        return pref.getString(KEY_EMAIL, null);
    }

    public boolean setAccount(Boolean log_account) {
        editor.putBoolean(LOG_ACCOUNT, log_account);
        return editor.commit();
    }

    public boolean isLogAccount() {
        return pref.getBoolean(LOG_ACCOUNT, false);
    }

    public boolean setPassword(String password) {
        editor.putString(KEY_PASS, password);
        return editor.commit();
    }

    public String getPassword() {
        return pref.getString(KEY_PASS, null);
    }

    public void setFirstTimeLaunch(boolean isFirstTime) {
        editor.putBoolean(IS_FIRST_TIME_LAUNCH, isFirstTime);
        editor.commit();
    }

    public boolean isFirstTimeLaunch() {
        return pref.getBoolean(IS_FIRST_TIME_LAUNCH, true);
    }

    public void setDemoUser(boolean demoOrNot) {
        editor.putBoolean(DEMO_USER, demoOrNot);
        editor.commit();
    }

    public boolean isDemoUser() {
        return pref.getBoolean(DEMO_USER, false);
    }

    public void loadLanguage(){
        SharedPreferences sharedPref = context.getSharedPreferences(SettingsFragment.KEY_PREF_LANGUAGE, Activity.MODE_PRIVATE);
        String actual_language = sharedPref.getString(SettingsFragment.KEY_PREF_LANGUAGE, "");
        Locale locale;
        switch (actual_language) {
            case "Italiano":
                locale = new Locale("it");
                actual_language = locale.getLanguage();
                break;
            case "Italian":
                locale = new Locale("it");
                actual_language = locale.getLanguage();
                break;
            case "Inglese":
                locale = new Locale("en");
                actual_language = locale.getLanguage();
                break;
            case "English":
                locale = new Locale("en");
                actual_language = locale.getLanguage();
                break;
            default:
                locale = new Locale(Locale.getDefault().getLanguage());
                actual_language = locale.getLanguage();
                break;
        }
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        context.getResources().updateConfiguration(config, null);
    }
}

