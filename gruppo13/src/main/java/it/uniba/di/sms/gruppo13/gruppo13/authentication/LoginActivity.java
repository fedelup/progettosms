package it.uniba.di.sms.gruppo13.gruppo13.authentication;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import java.util.Locale;
import java.util.Objects;

import it.uniba.di.sms.gruppo13.gruppo13.R;
import it.uniba.di.sms.gruppo13.gruppo13.data.PreferenceManager;
import it.uniba.di.sms.gruppo13.gruppo13.user.MainActivity;
import it.uniba.di.sms.gruppo13.gruppo13.user.SettingsFragment;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity {
    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    // UI references.
    private TextInputEditText mEmailView, mPasswordView;
    private FirebaseAuth auth;
    private ProgressDialog progressDialog;
    private PreferenceManager preferenceManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        auth = FirebaseAuth.getInstance();

        loadLanguage();

        setContentView(R.layout.activity_login);
        Objects.requireNonNull(getSupportActionBar()).setTitle(R.string.title_activity_login);

        progressDialog = new ProgressDialog(this);
        preferenceManager = PreferenceManager.getInstance(this);


        // Set up the login form.

        mEmailView = (TextInputEditText) findViewById(R.id.email);

        mPasswordView = (TextInputEditText) findViewById(R.id.password);

        if (preferenceManager.getPassword() != null) {
            mPasswordView.setText(preferenceManager.getPassword());
        }

        if (preferenceManager.getEmail() != null) {
            mEmailView.setText(preferenceManager.getEmail());
        }

        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;

                }
                return false;
            }
        });

        Button mEmailSignInButton = (Button) findViewById(R.id.login_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });
    }

    public String loadLanguage(){
        SharedPreferences sharedPref = getSharedPreferences(SettingsFragment.KEY_PREF_LANGUAGE, Activity.MODE_PRIVATE);
        String actual_language = sharedPref.getString(SettingsFragment.KEY_PREF_LANGUAGE, "");
        Locale locale;
        switch (actual_language) {
            case "Italiano":
                locale = new Locale("it");
                actual_language = locale.getLanguage();
                break;
            case "Italian":
                locale = new Locale("it");
                actual_language = locale.getLanguage();
                break;
            case "Inglese":
                locale = new Locale("en");
                actual_language = locale.getLanguage();
                break;
            case "English":
                locale = new Locale("en");
                actual_language = locale.getLanguage();
                break;
            default:
                locale = new Locale(Locale.getDefault().getLanguage());
                actual_language = locale.getLanguage();
                break;
        }
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        this.getResources().updateConfiguration(config, null);
        return actual_language;
    }


    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {
        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        } else if (TextUtils.isEmpty(password)) {
            mPasswordView.setError(getString(R.string.no_password));
            focusView = mPasswordView;
            cancel = true;
        }


        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            logIn(email, password);
        }
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 5;
    }

    private void logIn(final String email, final String password) {
        progressDialog.setMessage(getString(R.string.string_Progress_Dialog_Message));
        progressDialog.show();
        //authenticate user
        auth.signInWithEmailAndPassword(email.trim(), password)
                .addOnCompleteListener(LoginActivity.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            preferenceManager.setEmail(email);
                            preferenceManager.setPassword(password);
                            preferenceManager.setAccount(true);
                            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            finish();
                        } else {
                            Toast.makeText(LoginActivity.this, Objects.requireNonNull(task.getException()).getMessage(), Toast.LENGTH_LONG).show();
                            mPasswordView.setError(getString(R.string.error_incorrect_password));
                            mPasswordView.requestFocus();
                        }
                    }
                });
        progressDialog.dismiss();
    }
}
