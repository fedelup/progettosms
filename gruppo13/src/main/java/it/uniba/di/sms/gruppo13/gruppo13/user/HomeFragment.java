package it.uniba.di.sms.gruppo13.gruppo13.user;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;
import com.yarolegovich.discretescrollview.DiscreteScrollView;
import com.yarolegovich.discretescrollview.transform.ScaleTransformer;

import java.util.ArrayList;
import java.util.Objects;

import it.uniba.di.sms.gruppo13.gruppo13.R;
import it.uniba.di.sms.gruppo13.gruppo13.EstimateDriver;
import it.uniba.di.sms.gruppo13.gruppo13.authentication.RegistrationActivity;
import it.uniba.di.sms.gruppo13.gruppo13.car.Car;
import it.uniba.di.sms.gruppo13.gruppo13.car.GarageActivity;
import it.uniba.di.sms.gruppo13.gruppo13.car.adapter.DiscreteScrollViewAdapter;
import it.uniba.di.sms.gruppo13.gruppo13.data.DataStorage;
import it.uniba.di.sms.gruppo13.gruppo13.data.ManagerData;
import it.uniba.di.sms.gruppo13.gruppo13.data.obd.OBDConnectionActivity;

/**
 * Created by feder on 26/06/2018.
 */

public class HomeFragment extends Fragment {

    private View mView;
    private ArrayList<Car> arrayCar = new ArrayList<>();
    private boolean isCarChosen = false;
    private ProgressDialog progressDialog;
    private TextView percentage;
    private ManagerData managerData;
    private DataStorage dataStorage;
    private DiscreteScrollView discreteScrollView;
    private Context context;
    private EstimateDriver estimate;
    private static final String TAG = "HomeFragment";
    private CircularProgressBar circularProgressBar;

    private Car myCar;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_home, container, false);
        circularProgressBar = mView.findViewById(R.id.circularProgressBar);
        percentage = mView.findViewById(R.id.text_view_percentual);
        Button avvia_sessione = mView.findViewById(R.id.start_button);
        Button visualizza_storico = mView.findViewById(R.id.visualize);
        this.context = getContext();
        managerData = new ManagerData(context);
        if (managerData.getCurrentUser() != null)
            loadEntries();
        else {
            dataStorage = new DataStorage(context);
            if (dataStorage.loadCar() != null) {
                isCarChosen = true;
                dataStorage = new DataStorage(context);
                arrayCar.add(dataStorage.loadCar());
                loadGarage();
                if (dataStorage.loadSession().size() > 4) {
                    Snackbar snackbar = Snackbar
                            .make(getActivity().findViewById(R.id.fragment_container),
                                    "Limite sessioni raggiunto", Snackbar.LENGTH_INDEFINITE)
                            .setAction("REGISTRAZIONE", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    startActivity(new Intent(getActivity(), RegistrationActivity.class));
                                    getActivity().finish();
                                }
                            });
                    View snackbarView = snackbar.getView();
                    TextView textView = snackbarView.findViewById(android.support.design.R.id.snackbar_text);
                    textView.setTextColor(Color.WHITE);
                    snackbar.show();
                    avvia_sessione.setEnabled(false);
                }
            }
        }

        avvia_sessione.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isCarChosen) {
                    myCar = arrayCar.get(discreteScrollView.getCurrentItem());
                    Intent intentOBD = new Intent(getActivity(), OBDConnectionActivity.class);
                    intentOBD.putExtra("TARGA", myCar.getTarga());
                    startActivity(intentOBD);
                } else {
                    Snackbar snackbar = Snackbar
                            .make(getView(), "Nessun auto nel Garage", Snackbar.LENGTH_LONG)
                            .setAction("ADD", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    startActivity(new Intent(getActivity(), GarageActivity.class));
                                }
                            });
                    View snackbarView = snackbar.getView();
                    TextView textView = snackbarView.findViewById(android.support.design.R.id.snackbar_text);
                    textView.setTextColor(Color.WHITE);
                    snackbar.show();
                }
            }
        });

        visualizza_storico.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), LastSessionActivity.class));
            }
        });


        estimate = new EstimateDriver(context);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                int p;
                int surveys = estimate.getnRilevazioni();
                int infringements = estimate.getTotInfrazioni();
                try {
                    p = findPercentage(surveys, infringements);
                    int correttezza = (100-p);
                    percentage.setText(correttezza + "%");
                    circularProgressBar.setProgressWithAnimation(correttezza, 2500); // Default duration = 1500ms
                } catch (Exception e) {
                    e.printStackTrace();
                    percentage.setText(e.getMessage());
                }

            }
        }, 2500);

        return mView;
    }

    private void loadGarage() {
        discreteScrollView = mView.findViewById(R.id.picker);
        discreteScrollView.setItemTransitionTimeMillis(400);
        discreteScrollView.setItemTransformer(new ScaleTransformer.Builder()
                .setMinScale(0.8f)
                .build());
        discreteScrollView.setAdapter(new DiscreteScrollViewAdapter(context, arrayCar));
    }

    private void loadEntries() {
        progressDialog = new ProgressDialog(mView.getContext());
        progressDialog.setMessage(mView.getContext().getResources().getString(R.string.loading_car));
        progressDialog.setTitle(R.string.loading_car_title);
        progressDialog.setMax(50);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();
        // Read from the database
        DatabaseReference myRef = managerData.getRefDbCar();

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    arrayCar.clear();
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        Car car = new Car(
                                Objects.requireNonNull(snapshot.getKey()).trim(),
                                Objects.requireNonNull(snapshot.child("brand").getValue()).toString(),
                                Objects.requireNonNull(snapshot.child("model").getValue()).toString().trim(),
                                Objects.requireNonNull(snapshot.child("nickname").getValue()).toString());
                        arrayCar.add(car);
                    }
                }

                if (!arrayCar.isEmpty()) {
                    isCarChosen = true;
                    loadGarage();
                }
                progressDialog.dismiss();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                // Failed to read value
                Log.w(TAG, getString(R.string.errore_lettura_valori), error.toException());
                progressDialog.dismiss();
            }
        });
    }

    private Integer findPercentage(int surveys, int infringements) throws Exception {
        int result;
        if (surveys != 0) {
            if (infringements == 0)
                result = 100;
            else {
                result = (infringements * 100) / surveys;
            }
        } else {
            throw new Exception("NS");
        }
        return result;
    }
}
