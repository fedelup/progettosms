package it.uniba.di.sms.gruppo13.gruppo13.data.obd;

import android.Manifest;
import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ListView;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Objects;
import java.util.UUID;

import it.uniba.di.sms.gruppo13.gruppo13.CalculateErrors;
import it.uniba.di.sms.gruppo13.gruppo13.R;
import it.uniba.di.sms.gruppo13.gruppo13.bluetooth.BluetoothConnectionService;
import it.uniba.di.sms.gruppo13.gruppo13.bluetooth.adapter.DeviceListAdapter;
import it.uniba.di.sms.gruppo13.gruppo13.data.ManagerData;
import it.uniba.di.sms.gruppo13.gruppo13.data.PreferenceManager;
import it.uniba.di.sms.gruppo13.gruppo13.session.Session;

/**
 * Created by feder on 20/06/2018.
 */

public class OBDConnectionActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    private BluetoothAdapter mBluetoothAdapter;
    @SuppressLint("StaticFieldLeak")
    static BluetoothConnectionService mBluetoothConnection;
    private static final UUID MY_UUID = UUID.fromString("64ab0c11-6325-49f6-ab3e-41f959b55fa7");
    private static final String listRestoreState = "mBTList";
    private static final String buttonOutState = "buttonOut";
    private BluetoothDevice mBTDevice;
    public ArrayList<BluetoothDevice> mBTDevices = new ArrayList<>();
    public DeviceListAdapter mDeviceListAdapter;
    //private Session mSession;
    //private CoordinatorLayout coordinatorLayout;
    private static final int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 22;
    private boolean isButtonOut;
    private AppCompatButton btnBluetooth;


    //.private final ManagerData dao = new ManagerData();
    private StringBuilder messages;
    private ListView lvNewDevices;

    private String targa;
    private Button btnBack;
    private SwitchCompat switchDiscoverable;
    private SwitchCompat switchONOFF;

    private ManagerData managerData;

    private boolean isBroadcastRegistered1 = false;
    private boolean isBroadcastRegistered2 = false;
    private boolean isBroadcastRegistered3 = false;
    private boolean isBroadcastRegistered4 = false;
    private boolean isServiceOn = false;

    private PreferenceManager preferenceManager;

    // BroadcastReceiver per stato bluetooth (acceso / spento)
    private final BroadcastReceiver mBroadcastReceiver1 = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            // When discovery finds a device
            if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, mBluetoothAdapter.ERROR);

                switch (state) {
                    case BluetoothAdapter.STATE_OFF:
                        switchONOFF.setChecked(false);
                        break;
                    case BluetoothAdapter.STATE_TURNING_OFF:
                        switchONOFF.setChecked(false);
                        break;
                    case BluetoothAdapter.STATE_ON:
                        switchONOFF.setChecked(true);
                        break;
                    case BluetoothAdapter.STATE_TURNING_ON:
                        break;
                    default:
                        break;
                }
            }
        }
    };

    // BroadcastReceiver per modifiche agli stati del bluetooth (in ricerca, in connessione, ecc..)
    private final BroadcastReceiver mBroadcastReceiver2 = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();

            if (action.equals(BluetoothAdapter.ACTION_SCAN_MODE_CHANGED)) {

                int mode = intent.getIntExtra(BluetoothAdapter.EXTRA_SCAN_MODE, BluetoothAdapter.ERROR);

                switch (mode) {
                    //Device is in Discoverable Mode
                    case BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE:
                        break;
                    //Device not in discoverable mode
                    case BluetoothAdapter.SCAN_MODE_CONNECTABLE:
                        break;
                    case BluetoothAdapter.SCAN_MODE_NONE:
                        break;
                    case BluetoothAdapter.STATE_CONNECTING:
                        break;
                    case BluetoothAdapter.STATE_CONNECTED:
                        break;
                    default:
                        break;
                }

            }
        }
    };

    // BroadcastReceiver per tutti i dispositivi non ancora collegati
    private BroadcastReceiver mBroadcastReceiver3 = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();

            if (action.equals(BluetoothDevice.ACTION_FOUND)) {
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                if (!mBTDevices.contains(device)) {
                    mBTDevices.add(device);
                    mDeviceListAdapter = new DeviceListAdapter(context, R.layout.device_adapter_view, mBTDevices);
                    lvNewDevices.setAdapter(mDeviceListAdapter);
                }
            }
        }
    };

    // BroadcastReceiver per individuare i cambi di stato della connessione con dispositivi
    private final BroadcastReceiver mBroadcastReceiver4 = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();

            if (action.equals(BluetoothDevice.ACTION_BOND_STATE_CHANGED)) {
                BluetoothDevice mDevice = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

                if (mDevice.getBondState() == BluetoothDevice.BOND_BONDED) {
                    mBTDevice = mDevice;
                }
                if (mDevice.getBondState() == BluetoothDevice.BOND_BONDING) {

                }
                if (mDevice.getBondState() == BluetoothDevice.BOND_NONE) {

                }
            }
        }
    };


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putParcelableArrayList(listRestoreState, mBTDevices);
        outState.putBoolean(buttonOutState, isButtonOut);
        super.onSaveInstanceState(outState);


    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        mDeviceListAdapter = new DeviceListAdapter(this, R.layout.device_adapter_view, savedInstanceState.<BluetoothDevice>getParcelableArrayList(listRestoreState));
        lvNewDevices.setAdapter(mDeviceListAdapter);
        isButtonOut = savedInstanceState.getBoolean(buttonOutState);
        if (isButtonOut){
            btnBluetooth.setVisibility(View.GONE);
        } else {
            btnBluetooth.setVisibility(View.VISIBLE);
        }
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public void onBackPressed() {
        this.finish();
    }


    @Override
    protected void onDestroy() {
        if (isBroadcastRegistered1) {
            this.unregisterReceiver(this.mBroadcastReceiver1);
        }
        if (isBroadcastRegistered2) {
            this.unregisterReceiver(this.mBroadcastReceiver2);
        }
        if (isBroadcastRegistered3) {
            this.unregisterReceiver(this.mBroadcastReceiver3);
        }
        if (isBroadcastRegistered4) {
            this.unregisterReceiver(this.mBroadcastReceiver4);
        }
        super.onDestroy();


    }


    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        preferenceManager = PreferenceManager.getInstance(this);
        preferenceManager.loadLanguage();

        setContentView(R.layout.activity_obd_connection);

        final Intent serviceIntent = new Intent(OBDConnectionActivity.this, BackgroundOBDService.class);

        targa = getIntent().getStringExtra("TARGA");

        managerData = new ManagerData(this);
        isButtonOut = false;
        //coordinatorLayout = (CoordinatorLayout) findViewById(R.id.obd_coordinatorLayout);
        lvNewDevices = (ListView) findViewById(R.id.BT_NewDevices_lv);
        mBTDevices = new ArrayList<>();
        btnBack = (Button) findViewById(R.id.toolbar_btn_back);
        switchONOFF = (SwitchCompat) findViewById(R.id.switch_onoff);
        switchDiscoverable = (SwitchCompat) findViewById(R.id.switch_discoverable);
        btnBluetooth = (AppCompatButton) findViewById(R.id.btn_connect_bluetooth);
        messages = new StringBuilder();


        if (savedInstanceState != null) {
            if (Objects.requireNonNull(savedInstanceState.getParcelableArrayList(listRestoreState)).size() != 0) {
                mDeviceListAdapter = new DeviceListAdapter(this, R.layout.device_adapter_view, savedInstanceState.<BluetoothDevice>getParcelableArrayList(listRestoreState));
                lvNewDevices.setAdapter(mDeviceListAdapter);
                isButtonOut = savedInstanceState.getBoolean(buttonOutState);
                if (isButtonOut){
                    btnBluetooth.setVisibility(View.GONE);
                } else {
                    btnBluetooth.setVisibility(View.VISIBLE);
                }

            }

        }

        LocalBroadcastManager.getInstance(this).registerReceiver(mReceiver, new IntentFilter("incomingMessage"));

        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_BOND_STATE_CHANGED);
        registerReceiver(mBroadcastReceiver4, filter);
        isBroadcastRegistered4 = true;

        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        lvNewDevices.setOnItemClickListener(OBDConnectionActivity.this);

        if (mBluetoothAdapter.isEnabled()) {
            switchONOFF.setChecked(true);
        }

        btnBluetooth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getSupportFragmentManager();
                OBDDataDialog obdDataDialog = new OBDDataDialog();
                btnBluetooth.setVisibility(View.GONE);
                isButtonOut = true;

                mBluetoothConnection.write("AVVIOSESSIONE".getBytes(Charset.defaultCharset()));
                startService(serviceIntent);


                isServiceOn = true;
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.setCustomAnimations(R.anim.activity_open_enter, R.anim.activity_open_exit);
                transaction.add(R.id.obd_coordinatorLayout, obdDataDialog).addToBackStack(null).commit();
            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        switchONOFF.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    if (askPermission()) {
                        Intent enableBTIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                        startActivityForResult(enableBTIntent, 1);
                    } else {
                        switchONOFF.setChecked(false);
                    }


                }
                if (!isChecked) {
                    mBluetoothAdapter.disable();

                    IntentFilter BTIntent = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
                    registerReceiver(mBroadcastReceiver1, BTIntent);

                    switchDiscoverable.setChecked(false);
                }
            }
        });


        switchDiscoverable.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    if ((!mBluetoothAdapter.isEnabled() || !switchONOFF.isChecked()) && (!askPermission())) {
                        switchDiscoverable.setChecked(false);
                        switchONOFF.setChecked(false);
                    } else {
                        Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
                        discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 600);
                        startActivity(discoverableIntent);

                        IntentFilter intentFilter = new IntentFilter(BluetoothAdapter.ACTION_SCAN_MODE_CHANGED);

                        registerReceiver(mBroadcastReceiver2, intentFilter);
                        isBroadcastRegistered2 = true;

                        mBluetoothAdapter.startDiscovery();
                        IntentFilter discoverDevicesIntent = new IntentFilter(BluetoothDevice.ACTION_FOUND);
                        registerReceiver(mBroadcastReceiver3, discoverDevicesIntent);
                        isBroadcastRegistered3 = true;
                    }
                } else {
                    mBluetoothAdapter.cancelDiscovery();
                }
            }
        });


    }


    BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String text = intent.getStringExtra("theMessage");
            if (text.trim().equals("AVVIOSESSIONE")) {


                if (!isButtonOut) {
                    btnBluetooth.setVisibility(View.VISIBLE);
                    isButtonOut = true;
                }

            }

            if (isServiceOn) {
                if (text.trim().equals("DROPTHESERVICE")) {
                    final Intent serviceIntent = new Intent(OBDConnectionActivity.this, BackgroundOBDService.class);
                    ParseJSON parseJSON = new ParseJSON(messages.toString().trim());
                    Session s = parseJSON.CreateSession();
                    isServiceOn = false;
                    stopService(serviceIntent);
                    managerData.postSession(targa, s);
                    new CalculateErrors(context, s);
                    //startActivity(new Intent(OBDConnectionActivity.this, MainActivity.class));
                    finish();
                }

                if (text.startsWith("{") && text.endsWith("}")) {
                    messages.append(text);

                    Log.d("TAGFEDEPROVA", text);
                }
            }

        }
    };

    public void startConnection() {
        startBTConnection(mBTDevice, MY_UUID);

    }

    public void startBTConnection(BluetoothDevice device, UUID uuid) {
        mBluetoothConnection.startClient(device, uuid);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {
            if (resultCode == RESULT_CANCELED) {
                switchONOFF.setChecked(false);
                switchDiscoverable.setChecked(false);
            } else if (resultCode == RESULT_OK) {
                IntentFilter BTIntent = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
                registerReceiver(mBroadcastReceiver1, BTIntent);
                isBroadcastRegistered1 = true;
            }
        }

    }

    public void enableDisableBT() {
        if (mBluetoothAdapter == null) {
            //Eccezione non e' presente bluetooth
            throw new NullPointerException();
        }
        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableBTIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivity(enableBTIntent);

            IntentFilter BTIntent = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
            registerReceiver(mBroadcastReceiver1, BTIntent);
        }
        if (mBluetoothAdapter.isEnabled()) {
            mBluetoothAdapter.disable();

            IntentFilter BTIntent = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
            registerReceiver(mBroadcastReceiver1, BTIntent);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                // se la richiesta viene cancellata gli array sono vuoti
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permessi garantiti

                } else {
                    switchONOFF.setChecked(false);
                    switchDiscoverable.setChecked(false);
                    // permessi negati
                }
            }


        }
    }


    private boolean askPermission() {
        if (ContextCompat.checkSelfPermission(OBDConnectionActivity.this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {


            if (ActivityCompat.shouldShowRequestPermissionRationale(OBDConnectionActivity.this,
                    Manifest.permission.BLUETOOTH)) {
                return false;
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {
                // richiesta permessi
                ActivityCompat.requestPermissions(OBDConnectionActivity.this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
                return false;

            }
        } else {
            return true;
            // Permission has already been granted
        }
    }


    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        //conviene annullare la ricerca prima di collegare due dispositivi
        mBluetoothAdapter.cancelDiscovery();


        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
            mBTDevices.get(i).createBond();

            mBTDevice = mBTDevices.get(i);
            mBluetoothConnection = new BluetoothConnectionService(OBDConnectionActivity.this);
        }
        startBTConnection(mBTDevice, MY_UUID);
    }

}







