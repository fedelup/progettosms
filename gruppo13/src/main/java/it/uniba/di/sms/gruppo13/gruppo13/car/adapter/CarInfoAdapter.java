package it.uniba.di.sms.gruppo13.gruppo13.car.adapter;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPropertyAnimatorListener;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import it.uniba.di.sms.gruppo13.gruppo13.R;
import it.uniba.di.sms.gruppo13.gruppo13.car.Car;
import it.uniba.di.sms.gruppo13.gruppo13.car.DialogModifyCar;
import it.uniba.di.sms.gruppo13.gruppo13.car.GarageActivity;
import it.uniba.di.sms.gruppo13.gruppo13.data.ManagerData;
import jp.wasabeef.recyclerview.animators.holder.AnimateViewHolder;


public class CarInfoAdapter extends RecyclerView.Adapter<CarInfoAdapter.MyViewHolder> {
    private Context mContext;
    private List<Car> carList;
    private int positionCard;
    private ManagerData managerData;
    private FragmentManager fragmentManager;

    public class MyViewHolder extends RecyclerView.ViewHolder implements AnimateViewHolder {
        public TextView brand,modello, targa;
        public ImageView thumbnail, overflow;


        MyViewHolder(View view) {
            super(view);
            brand = view.findViewById(R.id.marca);
            modello = view.findViewById(R.id.modello);
            targa = view.findViewById(R.id.targa);
            thumbnail = view.findViewById(R.id.thumbnail);
            overflow = view.findViewById(R.id.overflow);
        }

        @Override
        public void preAnimateRemoveImpl(RecyclerView.ViewHolder holder) {

        }

        @Override
        public void animateRemoveImpl(RecyclerView.ViewHolder holder, ViewPropertyAnimatorListener listener) {
            ViewCompat.animate(itemView)
                    .translationY(-itemView.getHeight() * 0.3f)
                    .alpha(0)
                    .setDuration(300)
                    .setListener(listener)
                    .start();
        }

        @Override
        public void preAnimateAddImpl(RecyclerView.ViewHolder holder) {
            ViewCompat.setTranslationY(itemView, -itemView.getHeight() * 0.3f);
            ViewCompat.setAlpha(itemView, 0);
        }

        @Override
        public void animateAddImpl(RecyclerView.ViewHolder holder, ViewPropertyAnimatorListener listener) {
            ViewCompat.animate(itemView)
                    .translationY(0)
                    .alpha(1)
                    .setDuration(300)
                    .setListener(listener)
                    .start();
        }
    }

    public CarInfoAdapter(Context mContext, List<Car> carList, FragmentManager supportFragmentManager) {
        this.mContext = mContext;
        this.carList = carList;
        managerData = new ManagerData(mContext);
        fragmentManager = supportFragmentManager;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.car_card, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        Car car = carList.get(position);
        holder.brand.setText(car.getBrand());
        holder.modello.setText(car.getModel());
        holder.targa.setText(car.getTarga());

        // loading album cover using Glide library
        Glide.with(mContext).load(car.getThumbnail(mContext)).into(holder.thumbnail);

        holder.overflow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                positionCard = position;
                showPopupMenu(holder);
            }
        });


    }

    /**
     * Showing popup menu when tapping on 3 dots
     */
    private void showPopupMenu(MyViewHolder holder) {
        View view = holder.overflow;
        // inflate menu
        PopupMenu popup = new PopupMenu(mContext, view);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.menu_album, popup.getMenu());
        popup.setOnMenuItemClickListener(new MyMenuItemClickListener(holder));
        popup.show();
    }

    /**
     * Click listener for popup menu items
     */
    class MyMenuItemClickListener implements PopupMenu.OnMenuItemClickListener {

        private MyViewHolder myViewHolder ;
        private String targaAutoDelete;

        MyMenuItemClickListener(MyViewHolder holder) {
            myViewHolder = holder;
        }

        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {
            targaAutoDelete = carList.get(positionCard).getTarga();
            final int size = carList.size();

            Log.d("positionCard", Integer.toString(positionCard));
            Log.d("targaPos", carList.get(positionCard).getTarga());

            switch (menuItem.getItemId()) {
                case R.id.delete_car:
                    View view = ((GarageActivity) mContext).getWindow().getDecorView().findViewById(R.id.main_content);
                    Snackbar snackbar = Snackbar
                            .make(view, R.string.query_del, Snackbar.LENGTH_LONG)
                            .setAction(R.string.delete_car, new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    managerData.deleteCar(targaAutoDelete);
                                    carList.remove(positionCard);
                                    notifyItemRemoved(positionCard);
                                    notifyItemRangeChanged(positionCard, size);
                                }
                            });
                    View snackbarView = snackbar.getView();
                    TextView textView = snackbarView.findViewById(android.support.design.R.id.snackbar_text);
                    textView.setTextColor(Color.WHITE);
                    snackbar.show();

                    return true;

                case R.id.modify_car:
                    Bundle bundle = new Bundle();
                    bundle.putParcelable("car", new Car(
                            carList.get(positionCard).getTarga(),
                            carList.get(positionCard).getBrand(),
                            carList.get(positionCard).getModel(),
                            carList.get(positionCard).getNickname()));
                    managerData.deleteCar(targaAutoDelete);
                    carList.remove(positionCard);
                    notifyDataSetChanged();
                    DialogModifyCar dialogModifyCar = new DialogModifyCar();
                    dialogModifyCar.setArguments(bundle);
                    dialogModifyCar.show(fragmentManager, "sam");
                    return true;

                default:
            }
            return false;
        }
    }

    @Override
    public int getItemCount() {
        return carList.size();
    }
}
