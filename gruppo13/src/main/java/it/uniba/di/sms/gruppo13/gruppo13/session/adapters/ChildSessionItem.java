package it.uniba.di.sms.gruppo13.gruppo13.session.adapters;

import java.io.Serializable;

public class ChildSessionItem implements Serializable {

    private String childName;

    public String getChildName() {
        return childName;
    }

    public void setChildName(String childName) {
        this.childName = childName;
    }
}
