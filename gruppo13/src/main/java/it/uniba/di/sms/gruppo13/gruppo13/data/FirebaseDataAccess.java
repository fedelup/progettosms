package it.uniba.di.sms.gruppo13.gruppo13.data;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.List;

class FirebaseDataAccess {

    DatabaseReference myRef;
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    FirebaseAuth mAuth = FirebaseAuth.getInstance();
    String nomeDb = "Profilazione";


    boolean putData(List<String> percorso, Object obj ){
        myRef = database.getReference(nomeDb);
        for (String child : percorso) {
            myRef = myRef.child(child);
        }
        return myRef.setValue(obj).isSuccessful();
    }

    boolean deleteData(List<String> percorso){
        myRef = database.getReference(nomeDb);
        for (String child : percorso) {
            myRef = myRef.child(child);
        }
        return myRef.removeValue().isSuccessful();
    }

}
