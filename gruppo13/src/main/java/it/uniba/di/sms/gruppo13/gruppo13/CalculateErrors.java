package it.uniba.di.sms.gruppo13.gruppo13;

import android.content.Context;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import it.uniba.di.sms.gruppo13.gruppo13.data.DataStorage;
import it.uniba.di.sms.gruppo13.gruppo13.data.ManagerData;
import it.uniba.di.sms.gruppo13.gruppo13.data.obd.OBDStats;
import it.uniba.di.sms.gruppo13.gruppo13.session.Session;

public class CalculateErrors {
    private ManagerData managerData;
    private DataStorage dataStorage;
    private int nRilevazioni;
    private int totInfrazioni;

    public CalculateErrors(Context context, Session s) {
        managerData = new ManagerData(context);
        dataStorage = new DataStorage(context);

        int infrazioniNewSession = 0;
        ArrayList<OBDStats> stats = s.getOBDSession();
        for (OBDStats stat : stats) {
            String strada = stat.getTipoStrada();
            switch (strada) {
                case "StradaUrbana":
                    if (stat.getOBDSpeed() > 40) infrazioniNewSession++;
                    break;
                case "StradaExtraurbanaSec":
                    if (stat.getOBDSpeed() > 90) infrazioniNewSession++;
                    break;
                case "StradaExtraurbanaPrinc":
                    if (stat.getOBDSpeed() > 110) infrazioniNewSession++;
                    break;
                case "Autostrada":
                    if (stat.getOBDSpeed() > 130) infrazioniNewSession++;
                    break;
                default:
                    break;
            }
        }

        if (managerData.getCurrentUser() != null) {
            getDBValutation(infrazioniNewSession, s.getNumberRilevation());
        } else {
            getFileValutation(infrazioniNewSession, s.getNumberRilevation());
        }

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                managerData.postValutation(nRilevazioni, totInfrazioni);
            }
        }, 1500);

    }

    private void getFileValutation(final int infrazioniNewSession, final int nRil) {
        totInfrazioni = dataStorage.loadTotInfraction() + infrazioniNewSession;
        nRilevazioni = dataStorage.loadRilevazioni() + nRil;
    }

    private void getDBValutation(final int infrazioniNewSession, final int nRil) {
        DatabaseReference myRef = managerData.getRefValutation();
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    String ril = dataSnapshot.child("nRilevazioni").getValue().toString();
                    nRilevazioni = Integer.valueOf(ril);
                    String inf = dataSnapshot.child("totInfrazioni").getValue().toString();
                    totInfrazioni = Integer.valueOf(inf);
                    Log.d("nril", nRilevazioni + "");
                    Log.d("ninf", totInfrazioni + "");
                    nRilevazioni += nRil;
                    totInfrazioni += infrazioniNewSession;
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {                 //Failed to read value
            }
        });
    }
}

