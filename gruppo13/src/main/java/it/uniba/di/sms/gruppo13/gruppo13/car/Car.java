package it.uniba.di.sms.gruppo13.gruppo13.car;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

import it.uniba.di.sms.gruppo13.gruppo13.R;

public class Car implements Parcelable, Serializable {
    private String model;
    private String brand;
    private String targa;
    private String nickname;

    public Car(String targa, String brand, String model, String nickname) {
        this.model = model;
        this.brand = brand;
        this.targa = targa;
        this.nickname = nickname;
    }

    protected Car(Parcel input) {
        targa = input.readString();
        brand = input.readString();
        model = input.readString();
        nickname = input.readString();
    }

    public static final Creator<Car> CREATOR = new Creator<Car>() {
        @Override
        public Car createFromParcel(Parcel input) {
            return new Car(input);
        }

        @Override
        public Car[] newArray(int size) {
            return new Car[size];
        }
    };

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getTarga() {
        return targa;
    }

    public void setTarga(String targa) {
        this.targa = targa;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getThumbnail(Context current) {
        String brand = getBrand().replace("-", "")
                .toLowerCase().concat("_logo");
        int brand_logo = current.getResources().getIdentifier(brand, "drawable", current.getPackageName());
        if (brand_logo == 0) {
            brand_logo = R.drawable.car_image;
        }
        return brand_logo;
    }

    @Override
    public String toString() {
        return "Car{" +
                "model='" + model + '\'' +
                ", brand='" + brand + '\'' +
                ", targa='" + targa + '\'' +
                ", nickname='" + nickname + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(targa);
        parcel.writeString(brand);
        parcel.writeString(model);
        parcel.writeString(nickname);
    }
}
